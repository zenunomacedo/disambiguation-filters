\section{Introduction}
\label{sec:intro}
The evolution of programming languages in the 1960s was accompanied by
the development of techniques for the syntactic analysis of
programs. While techniques for processing text have evolved since
then, the general approach has remained the same. To define and
implement a new programming language, the general approach tends to be
the use of context-free grammars to specify the programming language
syntax. Then, a grammar-based tool, called parser generator,
automatically generates programs known as \textit{parsers}. Such
parsers are able to syntactically recognize whether a text is a
program in the specified programming language. Thus, for a parser to
be generated, a context-free grammar is needed. 
%Next, we show a context-free grammar for the usual arithmetic expression's notation, written in the widely-used YACC parser generator system~\citep{Johnson79yacc:yet}:
%\begin{lstlisting}
%exp    : term 
%       | exp '+' term 
%       | exp '-' term 
%       ;
%term   : factor 
%       | term '*' factor 
%       | term '/' factor 
%       ;
%factor : number 
%       | '(' exp ')'
%       ;
%\end{lstlisting}



From such grammar, a parser generator, such as the widely used YACC
parser generator system~\citep{Johnson79yacc:yet}, produces a parser
(implemented in a specific programming language) that given a text
(i.e. a sequence of characters) accepts/rejects it. If the text is
accepted a (abstract) syntax tree may be constructed. For unambiguous
grammars, a single tree is built, meaning that there is only one
possible way of accepting the text from such a grammar.
However, programmers often write ambiguous grammars. Firstly, because
they are easier to write/understand and evolve. Secondly, because
modern languages provide a ``cleaner'' syntax, which make programs
look nicer, but are easier to express by ambiguous grammars.

%%For example, the previous arithmetic expressions grammar can be re-written as follows:
%
%\begin{lstlisting}[label=grammar, caption=Grammar of arithmetic expressions, captionpos=t]
%exp    : exp '+' exp 
%       | exp '-' exp  
%       | exp '*' exp 
%       | exp '/' exp 
%       | '(' exp ')'
%       | number 
%       ;
%\end{lstlisting}
%The grammar presented in listing~\ref{grammar} is easier to understand
%and follows closely the syntax nature of arithmetic
%expressions. However, this grammar is ambiguous. It is trivial to see
%that the text \texttt{1+2*3} can produce two different trees, one that
%performs the addition first, and a second one that does the
%multiplication first.

Regular parser generators do not support ambiguous grammars. Thus, the
grammar writer has to provide the priority/associativity rules of the
ambiguous operators, in two ways: by refactoring the grammar to
eliminate ambiguity (which can be complex, and results in a more
complex and hard to understand grammar), or by providing the so-called
disambiguation rules, which are specified in the grammar. 
%For example, YACC supports the definition of
%disambiguation rules, thus, making it possible to define such ambiguous
%grammars and relying on disambiguation rules to generate a parser that
%is non-ambiguous and produces the desired syntax tree. 
%This is done by 
%rules such as these:
%
%\begin{lstlisting}[caption=Example of disambiguation rules, captionpos=t]
%%left '+' '-'
%%right '*' '/'  
%\end{lstlisting}
These disambiguation rules are pre-defined for most parser generators,
and are directly imbued into the parser itself when it is generated,
effectively modifying it. If the disambiguation rules are
well-defined, there will be no ambiguity problems and the parser will
be able to recognize text without any problem. However, There are
several problems with this approach:

\begin{itemize}

\item The only rules available are the pre-defined rules and they are
  not extensible: the parser generator itself would need to be updated
  in order to support new rules.
  
\item Because disambiguation rules are part of the grammar, they are
  context-free too. Thus, it is impossible to define context-dependent
  rules like for example to express that \texttt{'+'} operator has a
  different priority/associative when inside a while loop.
  
\item It is not modular. In fact, when the developer changes a
  disambiguation rule, the grammar changes, therefore a new parser
  must be generated.

%In the development cycle, it is possible that the
%  disambiguation rules need to be changed several times, and so the
%  parser itself will be generated several times. For big projects, the
%  generation of a parser is intensive performance-wise, and so the
%  generation of a parser several times has a noticeable impact on
%  development time.
%
\item Since the only rules available are the pre-defined rules, the
  developer is unable to observe the source code of these rules,
  instead opting to trust a black box that could potentially not
  behave as desired.

\end{itemize}

This paper presents an alternative to the classical approach, %refer to "this paper" directly?  
which does not suffer from these drawbacks:
Disambiguation rules are modular combinators that are kept separate
from the parser, being instead used as filters that are applied to the
results of parsing an input. In this way, changes to the
disambiguation rules do not affect the parser, allowing for an
efficient development cycle around disambiguation rules. Because we
express disambiguation rules as combinators, new rules can be easily
defined by combining existing ones. Moreover, our approach allows the
definition of context dependent disambiguation filters which behave
differently according to the context they are applied to.

\section{Grammars and parsers}

In the early ages of programming languages, it was usual to include
certain symbols in a language's grammar so that the generated parser
for that language would be more efficient. One such example is found
in the C programming language: the semicolon found at the end of each
instruction is a statement terminator~\citep{iso2011iec}. %\citep{Perlis:1981:SMA:578266}.

%It is used to resolve some ambiguities that could be found in the grammar. For example, without semicolons, the statement \verb|int a = b * c++| could be interpreted as \verb|int a = b; *c++;| or \verb|int a = (b * c++);|, both of which are valid C code. 

%In the following example, where an excerpt of C code is listed without semicolons, there are two possible ways to interpret it.
%
%\begin{lstlisting}[caption=Ambiguous C code, captionpos=t]
%int a = b * c++
%\end{lstlisting}
%\begin{lstlisting}[caption=One possible interpretation of such code, captionpos=t]
%int a = b; *c++;
%\end{lstlisting}
%\begin{lstlisting}[caption=Another possible interpretation of such code, captionpos=t]
%int a = (b * c++);
%\end{lstlisting}

%However, most recently, for the sake of allowing the developers to
%have more freedom and for them not to need to worry with the syntactic
%aspects of their programs,

Modern programming languages, however, tend to avoid the use of too
many syntactic symbols. This has the clear advantage that it not only
allows developers to write fewer symbols and less code while
programming, but also makes programs simpler and easier to
understand. Although it helps program comprehension, it also comes at
a price: it requires large and complex grammars and corresponding
parsers to handle them! In the next section we discuss grammars and
generalized parsers that can handle ambiguity.


%This report focuses on generic parsing techniques that generate a
%parser for any context-free grammar. Due to the ambiguity, these
%parsers produce various results for the same program, and
%disambiguation rules are used to select the desired solution, but the
%existing tools that provide generic parsing techniques are still
%limited.

%As such, a correct and efficient implementation of such disambiguation rules is desired. 

%These rules are to be implemented as combinators, which are simple code tools, that are easy to implement but very powerful when combined with other combinators. Having these rules implemented as combinators makes them much more versatile, as well as allowing for new rules to be created, by creating new combinators. Several performance tests are executed so as to measure the results obtained in this work.
