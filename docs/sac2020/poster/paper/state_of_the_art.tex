

While programming languages are usually specified via grammars, for
example using the BNF notation~\cite{conf/ifip/Backus59}, there are
various ways to generate a parser given such specification. Each
alternative method has its own advantages and disadvantages.  One of
the most well-known parser generators, YACC, uses an efficient LALR
parsing technique~\citep{Johnson79yacc:yet}: this relies on a
lightweight table-driven algorithm which was developed when runtime
and memory size were one of the main concerns. Other popular parser
generators include ANTLR~\citep{Parr:2011:LFA:1993498.1993548}, which
uses the ALL(*)~\cite{parr2014adaptive} algorithm, and
Happy~\citep{happy}, which produces Haskell source code. 


%Another example of a popular parser generator is ANTLR \citep{Parr:2011:LFA:1993498.1993548}, whose development started in 1989 as a LL(*) parser generator. The LL(*) algorithm allows for parsing decisions to be taken by looking at the following tokens in the input stream. ANTLR 4 uses the ALL(*)\cite{parr2014adaptive}, which is \textit{O(N\textsuperscript{4})} in theory but is shown to consistently perform linearly in practice.  

%While Yacc generates C code and ANTLR generates code for various programming languages, one of the most popular parser generators for Haskell is Happy \citep{happy}, which enables the developer to supply a file with the specification of a grammar, and in turn generates a parser, that is, a module of code that can read text according to that grammar's specifications. Happy is part of the Haskell Platform, being one of the most famous Haskell parsing tools. Due to its rather big popularity and regular maintenance, it is a fairly well optimized tool.

\subsection{Generalized Parsing}

Several parsing techniques do not deal with ambiguity properly. The
input is expected to be unambiguous, and when it is not, a certain
interpretation of such ambiguity is chosen so as to continue
parsing. This results in runtime-wise efficient but not so expressive
parsers, as they ignore any ambiguity problems that may arise.

Ambiguity can be dealt with using Generalized LR (GLR) parsing, which
just try all different parsing paths when an ambiguity occurs. Thus,
when faced with an ambiguous input, a GLR parser
\citep{Tomita:1985:EPN:537456} produces possible outputs instead of
selecting one of them. That is to say that it produces a set of
abstract syntax trees, called parse forest, and not a single one. As a
consequence, they are slower than their non-generalized counterparts,
due to their additional flexibility in dealing with non-determinism.
If no non-determinism is present, a GLR parser will be as efficient as
a regular LR parser~\citep{GPsomecosts}.


%With the constant advances in technology, the limitations that made this technique undesirable are gone and there are parser generators that allow the use of the GLR algorithm, such as Happy. 

%However, GLR is not the only generalized algorithm. The GLL algorithm \cite{scott2010gll} \cite{scott2013gll} is also generalized, but much less explored. 
%This algorithm is worst-case cubic in both time and space and there are possible optimizations to it\cite{Afroozeh2015}. 

\subsection{Scannerless Parsing}

%Generally speaking, parser applications are divided in two components: the lexer and the parser. The lexer takes the input and breaks it into a list of tokens, and then the parser takes those tokens and matches them with the production rules to produce the actual parsing result. 

Grammars usually rely on regular expressions to specify their terminal
symbols, which are then processed via very efficient finite state
automata-based recognizers~\citep{Saraiva02b}, the so-called
scanners~\citep{Johnson79yacc:yet}. The use of scanners
provides also some form of modularity in grammar/parser
writing. However, this approach has a severe limitation: because
some parts of the language are defined outside the BNF grammar
formalism, and handled by a scanner, there is not an unified and
coherent way of processing all symbols of the language.

Scannerless parsing~\citep{Salomon:1989:SNP:73141.74833} consists of
skipping the lexer-phase entirely and treating each character from the
input as a token, which is directly processed by the parser. As a
consequence, every program's character is processed by the
parser, and not by an external lexer. Usually, terminal grammar symbols
are specified via regular expressions, still, but they are transformed
into an equivalent regular CFG before a parser is produced. There are
two key advantages in this approach: First, ambiguous grammars are
compositional, and as a result two grammars can be merged and a
(ambiguous) parser can be generated, which will not be the case when
merging regular expressions. Second, because all grammar symbols are
handled in the same way, \textit{i.e. via a parser}, advanced parsing
techniques can be applied to all symbols. This is the case, for
example, of the disambiguation rules that we discuss in the next section.


\section{Disambiguation Filters for Scannerless Generalized Parsing}
\label{sec2.5}

Scannerless Generalized parsers handle ambiguous grammars and
inputs. Moreover, they handle all grammar symbols in a uniform and
canonical way, not relying on external recognizers to process part of
the input. Since they deal with ambiguous grammars/inputs, it is
expected that they generate a set of outputs as a result, which
represent all possible interpretations. However, not all possible
interpretations are desired: depending on the situation, a developer
might want to only get one or a small subset of parse trees.

The task of processing the set of ambiguous parse trees generated by a
parser and eliminating the undesired ones is called
disambiguation. Typically, such filtering is done on the parser
itself, by adding disambiguation rules or by modifying part of the
parser, so that the undesired interpretations cannot be produced.
When dealing with scannerless parsing new disambiguation rules are
needed. In this section, we present a set of disambiguation filters
following the the work of van den Brand \textit{et
  al.}~\citep{vandenBrand2002}.

%\subsection{Priority and Associativity Filter}
%

The \textbf{priority} filter specifies that certain
productions have a higher priority than others, while the
\textbf{associativity} filter specifies that an operator associates left or
right. These filters can be specified as annotations in the
productions they refer to.

%As such, extending the grammar of arithmetic expressions seen in listing~\ref{grammar}, these filters could be described as shown in listing \ref{grammarPA}.
%
%\begin{lstlisting}[label=grammarPA, caption={Grammar of arithmetic expressions, with priority and associativity filters}, captionpos=t]
%Exp :         Exp '+' Exp   %left
%            | Exp '-' Exp   %left
%            | Exp '*' Exp   %left
%            | Exp '/' Exp   %left
%            | '(' Exp ')' 
%            | int             
%
%{Exp : Exp '*' Exp} > {Exp : Exp '+' Exp}
%\end{lstlisting}
%
%
%
%\subsection{Reject Filter}

The \textbf{reject} filter enables the creation of keywords in the
grammar. In other words, it rejects some productions from deriving
into certain sequences. This is extremely useful as in most
programming languages, some keywords cannot be used as variable names,
and this filter allows for a clean implementation of this
incompatibility. For example, in the C language, it is not allowed for a
variable to have the name of the reserved keyword \textit{"while"}.


%A developer could specify such filters by describing what the production should not derive into, as shown in listing \ref{grammarRej}.
%
%\begin{lstlisting}[label=grammarRej, caption={Reject filter for "while" reserved keyword}, captionpos=t]
%Identifier : "while" {reject}
%\end{lstlisting}
%
%\subsection{Follow Filter}


The \textbf{follow} filter (also known as longest match filter) solves
a less obvious ambiguity that arises in scannerless parsing. When the
grammar dictates that a sequence of symbols can be parsed using one
single production or a sequence of productions, for example, a
sequence of digits which could be read as a single number or several
numbers with no separators, the follow filter specifies that the
longest match is to be performed.

%As an example, for the input
%\textit{"12 3"}, this filter can be used to specify that we want to
%parse it as a sequence of two numbers, and not as a sequence of three
%numbers.

%Let us consider as example the grammar specified in listing~\ref{grammarFollow}, which specifies a list of values, separated by whitespaces, in which a whitespace is defined as zero or more whitespace characters such as spaces and tabs. This example can be used to define the grammar for vector declarations of Matlab, where a vector can be declared as a list of values using only whitespace as separators. 
%
%\begin{lstlisting}[label=grammarFollow, caption={Grammar for a list of values}, captionpos=t]
%Values :     Values Ws Values
%           | Value
%
%Ws : [ \\n\\t]*
%Value : [0-9]+
%\end{lstlisting}
%When the input is a single number with various digits, such as "37", it is ambiguous. It can be interpreted as a single "Value", or as two separated "Values", separated by exactly zero "Ws". The second interpretation is generally unwanted, and a way to fix this would be to change the definition of "Ws" to only allow for one or more whitespaces. However, when "Ws" is the default whitespace definition used throughout the grammar, this change is undesired as it would force the grammar to be changed to accomodate this change. 
%
%Hence, in this case, the follow filter is defined to specify that a "Value" cannot be followed by a digit. This way, "Value" must contain all the digits before an actual whitespace occurs, therefore forcing a longest match to occur. For the example input "37", the second interpretation where two separated "Values" contain each one digit would be invalid, as the first "Values" string must be followed by a non-digit character, but is followed by the "7" digit. 
%
%\begin{lstlisting}[label=filterRej, caption={Follow filter for the grammar for a list of values}, captionpos=t]
%Value -/- [0-9]
%\end{lstlisting}
%
%\subsection{Preference Filter}

\label{preffilter}
When there are several correct input interpretations, but some are
preferred to others, a \textbf{preference} filter is used. It
specifies which parse results should be removed when there are several
correct outputs but the developer wants to select only some of them.
This filter is used to disambiguate the dangling else problem,
which can be exemplified by the input \textit{if bool1 then if bool2 then out1 else out2} that can be interpreted in two ways,\textit{if bool1 then (if bool2 then out1 else out2)} or \textit{if bool1 then (if bool2 then out1) else out2}. 

%\begin{lstlisting}[label=grammarPref, caption={Grammar exemplifying the dangling else problem, and usage of preference filter}, captionpos=t]
%Term :  "if" Bool "then" Term 
% | "if" Bool "then" Term "else" Term %prefer
%\end{lstlisting}
