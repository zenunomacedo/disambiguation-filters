\section{Introduction}
The evolution of programming languages in the 1960s was accompanied by
the development of techniques for the syntactic analysis of
programs. While techniques for processing text have evolved since
then, the general approach has remained the same. To define and
implement a new programming language, the general approach tends to be
the use of context-free grammars to specify the programming language
syntax. Then, a grammar-based tool, called parser generator,
automatically generates programs known as \textit{parsers}. Such
parsers are able to syntactically recognize whether a text is a
program in the specified programming language. Thus, for a parser to
be generated, a context-free grammar is needed. Next, we show a
context-free grammar for the usual arithmetic expression's notation,
written in the widely-used YACC parser generator system~\citep{Johnson79yacc:yet}:
\begin{lstlisting}
exp    : term 
       | exp '+' term 
       | exp '-' term 
       ;
term   : factor 
       | term '*' factor 
       | term '/' factor 
       ;
factor : number 
       | '(' exp ')'
       ;
\end{lstlisting}
From such grammar, a parser generator (like YACC) produces a parser
(implemented in a specific programming language) that given a text
(ie. a sequence o characters) accepts/rejects it. If the text is
accepted a (abstract) syntax tree is constructed. For unambiguous
grammars, like the one presented above, a single tree is built, meaning
that there is only one possible way of accepting the text from such a
grammar.

However, programmers often write ambiguous grammars. Firstly, because
they are easier to write/understand and evolve. Secondly, because
modern languages provide a ``cleaner'' syntax, which make programs
look nicer, but requires the grammars to handle ambiguities.
For example, the previous arithmetic expressions grammar can be
re-written as follows:

\begin{lstlisting}
exp    : exp '+' exp 
       | exp '-' exp  
       | term '*' exp 
       | term '/' exp 
       | '(' exp ')'
       | number 
       ;
\end{lstlisting}
This grammar is easier to understand and follows closely the syntax
nature of arithmetic expressions. However, this grammar is
ambigous. It is trivial to see that the text \texttt{1+2*3} can produce
two different trees, one that performs the addition first, and a
second one that does the multiplication first. In the previous grammar,
the priority of the arithmetic operators were expressed in the grammar
itself.

In order to have YACC producing a parser for this second grammar, we
need to provide the priority/associtivity rules of the operators. In
fact, YACC supports the definition of disambiguation rules, thus,
making possible to define ambigous grammars and relying on
disambiguation rules to generate a parser that is non-ambigous and
produces the desired syntax tree. This is done by the folowing rules:

\begin{lstlisting}
%left '+' '-'
%right '*' '/'  
\end{lstlisting}
Such disambiguation rules are pre-defined for most parser generators,
and are directly imbued into the parser itself when it is generated,
effectively modifying it. If the disambiguation rules are
well-defined, there will be no ambiguity problems and the parser will
be able to recognize text without any problem. There are several
problems with this approach:

\begin{itemize}

\item The only rules available are the pre-defined rules and they are
  not extensible, therefore not allowing the developer to produce new
  rules that might be required for solving different problems.

\item When the developer intends to change a disambiguation rule, the
  grammar must be changed, therefore a new parser must be
  generated. In the development cycle, it is possible that the
  disambiguation rules need to be changed several times, and so the
  parser itself will be generated several times. For big projects, the
  generation of a parser is not a trivial step, and so the generation
  of a parser several times has a noticeable impact on development
  time.

\item Since the only rules available are the pre-defined rules, the
  developer is unable to observe the source code of these rules,
  instead opting to trust a black box that could potentially not
  behave as desired.

\end{itemize}   
This paper presents an alternative to the classical approach, %%refer to "this paper" directly?
which does not suffer from the same problems. In the approach
presented in this paper, disambiguation rules are modular
combinators that are kept separate from the parser, being instead used
as filters that are applied to the results of the parser's
recognition. This way, changes to the disambiguation rules do not
affect the parser, allowing for an efficient development cycle around
disambiguation rules.

\subsection{Motivation}

In the early ages of programming languages, it was usual to purposely include certain symbols in a language's grammar so that the generated parser for said language was more efficient. 

The most obvious example is found in the C programming language: the semicolon found at the end of each instruction is a statement terminator \citep{Perlis:1981:SMA:578266}. It is used to resolve some ambiguities that could be found in the grammar. In the following example, where an excerpt of C code is listed without semicolons, there are two possible ways to interpret it.

\begin{lstlisting}[caption=Ambiguous C code, captionpos=t]
int a = b * c++
\end{lstlisting}
\begin{lstlisting}[caption=One possible interpretation of such code, captionpos=t]
int a = b; *c++;
\end{lstlisting}
\begin{lstlisting}[caption=Another possible interpretation of such code, captionpos=t]
int a = (b * c++);
\end{lstlisting}    
However, most recently, for the sake of allowing the developers to have more freedom and for them not to need to worry with the syntactic aspects of their programs, modern programming languages tend to avoid the use of too many syntactic symbols. This has the clear advantage that it not only allows developers to write fewer symbols and less code while programming, but also makes programs simpler and easier to understand. Although it helps program comprehension, it requires complex grammars and corresponding parsers to handle such programs. 

This report focuses on generic parsing techniques that generate a parser for any context-free grammar. Due to the ambiguity, these parsers produce various results for the same program, and disambiguation rules are used to select the desired solution, but the existing tools that provide generic parsing techniques are still limited. 

As such, a correct and efficient implementation of such disambiguation rules is desired. These rules are to be implemented as combinators, which are simple code tools, that are easy to implement but very powerful when combined with other combinators. Having these rules implemented as combinators makes them much more versatile, as well as allowing for new rules to be created, by creating new combinators. Several performance tests are executed so as to measure the results obtained in this work.
