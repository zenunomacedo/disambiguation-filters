\section{Performance Analysis}
The current implementation of the work presented in this paper is available at the url \url{https://bitbucket.org/zenunomacedo/disambiguation-filter/}. In it, the examples and the combinators toolbox used in this paper are available for experimentation, as well as the grammar specifications for the different languages used. 

For a given HaGLR parser, the \textit{GLRActionsTable} module contains the main functions that perform the actual parsing. It is important to note the function \textit{glr\_parser\_aterm}, as this function takes a string as input and outputs a parse forest, according to the grammar used to generate the parser.

Now, an input string is needed. To display all the disambiguation rules presented before, a bigger input string is used. The following input is a toy program built specifically for testing the filters, that runs through an array and checks if it contains an even number. 
\begin{lstlisting}[label=filterin]
int [] input = [ 11 22 33 4+73+2 ];
bool found = false; int counter = 0;
while (counter < 4){
  if found == false 
    then if input[counter] % 2 == 0
      then found = true; else break; }
\end{lstlisting}
It contains all the ambiguities presented before:
\begin{itemize}
\item Follow - some digit sequences inside the array declaration can be interpreted by the parser as either one number or two numbers.
\item Associativity - the last value in the array is a sum of three numbers, which can be associative to the left or right. 
\item Reject - the keywords \textit{true}, \textit{false} and \textit{break} can be interpreted as identifiers. 
\item Priority - the expression \textit{"input[counter] \% 2 == 0"} is ambiguous as there is no set priority for the equality and module operations. 
\item Preference - there is a dangling \textit{else} keyword that is not immediately associated to either of the \textit{if} clauses, resulting in ambiguity.
\end{itemize}

All the benchmarks present in this work were run using the Criterion \cite{criterion} library with the default settings, which runs the benchmarks several times and returns the average runtime. The benchmarks were run on a system described in table \ref{table:specifications}.

\begin{table}[h]
\begin{center}
\begin{tabular}{|l|c|}
\hline
\textbf{Processor} & \textbf{\begin{tabular}[c]{@{}c@{}}Intel® Core™ i5-7200U Dual-Core, \\ 2.50 GHz, 3MB cache\end{tabular}} \\ \hline
Hard Disk          & SSD 256 GB PCIe® NVMe™ M.2                                                                               \\ \hline
RAM                & 8GB SDRAM LPDDR3-1866                                                                                    \\ \hline
OS                 & Ubuntu 16.04 LTS                                                                                         \\ \hline
\end{tabular}
\caption{Specifications of the hardware used for the tests.}
\label{table:specifications}
\end{center}
\end{table}
\vspace{-1.5em}

Three grammars are available in the repository. The simplest is the \textit{Small} grammar, and it consists of simple arithmetic expressions and declaration of variables. The \textit{Filters} grammar, present in previous examples throughout this work, contains several possible expressions and ambiguities, behaving as a toy programming language. The \textit{Tiger} grammar, can be used to generate a parser for the Tiger programming language and is a real-world example that is used to show how this work behaves when presented with a bigger grammar. 

For the \textit{Filters} and \textit{Tiger} languages, the ambiguous input shown in listing \ref{filterin} and the solution to the n-Queens problem (available in the repository) were used, respectively. They were chosen as the first one is known to be ambiguous, since it was built for such purposes, and the last one is a real-world example. 

For the \textit{Filters} language, the filters used are the ones presented throughout this dissertation. For the \textit{Tiger} language, a reduced subset of filters was used. These do not provide disambiguation for the whole language, but are enough for this particular example. This was done to provide a more fair comparison with the \textit{Filters} example, which also only uses a subset of disambiguation filters comprehensive enough for the example at hand. 

\begin{lstlisting}
filters :: TFilter String
filters = gen_filters [LeftAssoc "&" 
        , ["+"] `Before` ["-"]
        , ["&"] `After` ["=", ">", "<", ">=", "<="]]

disambiguation :: TFilter String
disambiguation = every filters
\end{lstlisting}
For each of the languages, it was measured, in seconds, the time it takes to parse without and with disambiguation filters. For the \textit{Filters} input, the use of disambiguation filters actually reduces the time it takes to parse the input. This happens due to the laziness of Haskell - while the parsing and disambiguation steps, in theory, are separated, the actual result is that they intertwine and therefore unnecessary computations are thrown out by the filters. However, for the \textit{Tiger} input, which is more complex but less ambiguous, the filters actually increase the execution time, as they add more computational weight to the process and do not benefit from lazy computations as much.

\begin{table}[h]
\begin{center}
\begin{tabular}{|l|l|l|}
\hline
\textbf{}             & \textbf{Filters} & \textbf{Tiger} \\ \hline
\textbf{No filters}   & 0.436            & 2.119          \\ \hline
\textbf{With filters} & 0.329            & 2.829          \\ \hline
\end{tabular}
\caption{Comparison of the runtime without and with filters, in seconds, for the Filters and Tiger parsers.}
\label{table:filter}
\end{center}
\end{table}
\vspace{-1.5em}

We also compared the these methods with performance obtained by using an unambiguous grammar, which in theory should generate an efficient parser. For this, the \textit{Small} grammar was used, and variations of the following ambiguous input were used. This input is built upon the notion that arithmetic expressions are rather common in programs, and can be highly ambiguous if there is a lack of usage of symbols such as parenthesis, which remove those ambiguities. 

\begin{lstlisting}[label=smallExpr]
1+2*3+4*5+6*7+8*9+1+2*3+4
\end{lstlisting}
Table \ref{table:small} was built to show the results obtained, and each of the columns represents the number of possible interpretations with an ambiguous parser. This means, for example, that the input corresponding to the column labeled \textit{16796} has this number of different interpretations. This represents a problem in terms of performance as constructing just one interpretation is much more efficient, and for large inputs, this overhead quickly becomes overwhelming. 
\begin{table}[h]
\begin{center}
\begin{tabular}{|l|l|l|l|}
\hline
                     & \textbf{16796}                & \textbf{58786}                & \textbf{208012}               \\ \hline
\textbf{Ambiguous}   & 0.376                         & 1.282                         & 4.805                         \\ \hline
\textbf{Unambiguous} & 1.04 x 10\textsuperscript{-4} & 1.19 x 10\textsuperscript{-4} & 1.21 x 10\textsuperscript{-4} \\ \hline
\end{tabular}
\caption{Comparison of the runtime obtained by using a Small parser, in seconds, for an unambiguous and ambiguous variants.}
\label{table:small}
\end{center}
\end{table}
\vspace{-1.5em}
The results displayed in this table easily prove that this work is not apt for performance-intensive solutions. The ambiguous parser takes several seconds to parse a simple arithmetic expression, and the execution time grows rather quickly with the increase of complexity of the expression. The unambiguous parser takes less than a millisecond to parse the same expression, and the increase in runtime with bigger expressions is barely noticeable. 