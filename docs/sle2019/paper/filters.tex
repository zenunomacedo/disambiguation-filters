\section{Embedded Disambiguation Filters}
\label{chapter:ps}


%The classical approach to disambiguation is done by defining the disambiguation rules in the grammar description very much like yacc-like tools do. However, this is not modular at all, as every small change in the grammar implies generating a new parser, since the disambiguation rules are meshed into the parser during parser generation, changing the parser. 

In this paper, a new approach for parser disambiguation is described. Instead of expressing the
disambiguation rules in the parser itself, they are kept separate. The
parser is generated once, and it produces a possibly ambiguous
result. Afterwards, the disambiguation rules are applied to the forest of AST, removing
some or all of the ambiguities, according to what the developer
specified. There are several advantages and disadvantages to using this
process instead of the classical approach. Since the parser that is
used is unmodified, it is less efficient, as the classical approach
removes parts of the parser reducing thus the number of results the
parser has to output. However, while the parser itself is less
efficient, the development cycle of the developer is more efficient,
as there is no need to constantly produce a new parser after a change in a disambiguation rule. Only the
disambiguation rules are to be changed, and this can be easily done if
the implementation is user-friendly. Therefore, the disambiguation
rules are implemented as filter combinators, where the developer
starts with basic blocks that perform very simple filtering, combining
them in easy-to-understand ways to produce complex filters that
perform the desired disambiguation rules.

While this solution is less efficient, it allows for a faster
development cycle, and the final version of the product could use the
classical approach to produce an efficient parser with disambiguation
rules, after this approach was used to define which disambiguation
rules should be used. 


\subsection{Abstract Syntax Trees}

A parser typically constructs the parsing result as a syntax tree. This is a tree that contains all the information from the input categorized in accordance to the grammar. %Example needed
As an example, according to the grammar seen in \ref{a}, for an input "1+2", the output can be seen in figure \ref{fig:SyntaxTree1}. However, for an input "1+2*3", and with no disambiguation rules defined, the parser does not understand arythmetic priority and can generate two possible outputs, as seen in figure \ref{fig:SyntaxTree2}. For a generalized parser, both possibilities are generated, and therefore the output is not a tree but a \textbf{\textit{forest}}, that is, a list of trees. 

Since, in this work, the disambiguation is to be performed after the parsing step, the disambiguation rules should be applied to the syntax forest, trying to find undesired patterns in them and removing them if any of these patterns are found. Such process will be described in more detail in the following sections.


\begin{figure}
    \begin{center}
    %\includegraphics[width=\columnwidth]{img/grammar.png}
    \includegraphics[scale=0.75]{img/SyntaxTree1.png}
    \caption{Syntax tree for the input "1+2".}
    \label{fig:SyntaxTree1}
    \end{center}
\end{figure}

\begin{figure}
    \begin{center}
    %\includegraphics[width=\columnwidth]{img/grammar.png}
    \includegraphics[scale=0.66]{img/SyntaxTree2.png}
    \caption{Syntax trees for the input "1+2*3".}
    \label{fig:SyntaxTree2}
    \end{center}
\end{figure}

\subsection{Haskell XML Toolbox}
Syntax trees are generalized trees which can represent a program. Generalized trees are often called Rose Trees in the functional programming setting and are well studied in several contexts. One of them is XML, for which there are several generic tools that can be used. In this work, the filter variant of the Haskell XML Toolbox \citep{hxt} is used as a base for building combinators for filtering syntax trees. 

In this library, Rose Trees are known as \textit{NTree}, and these trees and some tools for manipulating them are defined in the module \textit{Data.Tree.NTree.TypeDefs}. The building blocks used in this work are defined in the module \textit{Data.Tree.NTree.Filter}. 


\subsection{HaGLR Parser Generator}
The HaGLR tool \citep{haskellglrgood} is a Haskell implementation of a GLR parser generator, which was implemented with pedagogic purposes which conflict with performance ones. In fact, it is currently extremely inefficient, as shown in Fernandes et al.' work, where AG.bib, a large bibliographic database of attribute grammars related literature (compiled and available at INRIA) was fed as input to both a Happy parser and a HaGLR parser, and the Happy parser was much more efficient. 

Since performance is not our main focus and HaGLR is a generalized parser generator, it is adequate for this work. It produces as result a pure parse tree forest, which is a list of parse trees. This is not the case for all generalized parsers as some optimizations change the representation of the parse forests to save memory, which use a different, more compact approach, but are less intuitive to work around. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsection{Basic Combinators}

To be able to build complex filters to disambiguate the result of a parser, some basic combinators to build upon are needed. Most of the combinators described in this section are defined in the Haskell XML Toolbox library. They enable the creation of filters, as well as manipulation and composition. Some new combinators were also created to better fit the needs of this work. They are available in the repository of this work, but as they are simple and intuitive, they are not described in this paper. 


\subsection{Disambiguation Filters}
Having defined these Rose Tree filter combinators, we have all ingredients to implement the aforementioned disambiguation filters. In the following sections, the types of filters described in section \ref{sec2.5} are implemented using these combinators. However, it is important to note that they apply to the parse trees produced by the parser, and if a different parser is used, it might be needed to change the filters accordingly. 

To build a filter that defines disambiguation rules, it is first needed to take a look at a parse tree and devise an algorithm for checking if it is a valid parse tree. To do so, it is important to understand the structure of the parse trees produced by the parser.

In the remaining of this section, we will use the expression grammar \textit{Filters} (available online at this work's repository) as running example.


\subsubsection{Associativity Filter} 
Given a parser for the \textit{Filters} grammar, and the simple input string \textit{"1+2+3"}, the output consists of two parse trees, as seen in figure \ref{fig:parseresultsA}. The left one shall be considered the correct interpretation and the right one the undesired interpretation for this example. As such, to write the filter, it is needed to locate what is the pattern that stands out in this example, and then describe a way to remove it using the combinators. 

\begin{figure}
    \begin{center}
    %\includegraphics[width=\columnwidth]{img/grammar.png}
    \includegraphics[width=\columnwidth]{img/parseresultsA.png}
    \caption{Simplified parse trees for the string "1+2+3;".}
    \label{fig:parseresultsA}
    \end{center}
\end{figure}
The names of the nodes match with the names of the productions. As an example, the \textit{Exp1} and \textit{Exp2} nodes refer to the \textit{Exp} production. \textit{NTfromT2} refers to a terminal symbol, in this case, the \textit{+} sign. Finally, \textit{InfixExp2} refers to the \textit{InfixExp} production, which is the one that defines the additions. In this case, the difference between the two parse trees is in whether an \textit{InfixExp2} node is to the left or to the right of their parent \textit{InfixExp2} node. This represents the ambiguity, in which the addition can be left-associative or right-associative. Therefore, the ambiguity can be solved by implementing a filter that removes any tree where there is an \textit{InfixExp2} node to the right (or left) of another \textit{InfixExp2} node.

The implementation of this filter is rather trivial once the algorithm is defined. This filter will look at the root node, check if it is an \textit{InfixExp2}, and, if it is, check if there is an \textit{InfixExp2} node in the right child of said node. If there is not, then the tree is correct according to the filter. 

\begin{lstlisting}
associativity :: TFilter String
associativity = neg rightNodeCheck `when` matches "InfixExp2"
 where rightNodeCheck = matches "InfixExp2" . head . getChildren . (!!2) . getChildren
\end{lstlisting}
Therefore, the filter is finished and can be read in a reasonably easy way. When the root matches the string \textit{"InfixExp2"}, the rightmost child must not match the string \textit{"InfixExp2"}. Of course, when the root does not match this string, the filter does nothing. 

However, this filter does not work as expected on a real parse tree. That is because the filter only applies to the root, and not to the whole tree. In reality, the ambiguity can exist deep into the tree, and so the \textit{every} combinator is needed to apply the filter to all the tree, and discard the tree if any of the nodes fail to satisfy it. 

\begin{lstlisting}
disambiguationAssoc :: TFilter String
disambiguationAssoc = every associativity 
\end{lstlisting}

One last step that can be taken is to generalize the resulting
filter. This can be done by having the node's name passed as an
argument, and not have it hard-coded in the definition. The
associativity filter is also generalized to handle associativity to
the left and to the right.

\begin{lstlisting}
left_assoc, right_assoc :: String -> TFilter String
left_assoc  p = neg (matches p . head . getChildren . (!!2) . getChildren) `when` matches p
right_assoc p = neg (matches p . head . getChildren . (!!0) . getChildren) `when` matches p

disambiguationAssocGeneric :: TFilter String
disambiguationAssocGeneric = every (left_assoc "InfixExp2") 
\end{lstlisting}


\subsubsection{Priority Filter} 

The priority filter, in its essence, is quite similar to the
associativity filter. Given a parser for the \textit{Filters} grammar, 
and the simple input string
\textit{"1+2*3"}, the output consists of two parse trees, as seen in
figure \ref{fig:parseresultsP}. As before, there is a pattern that is
undesired and that should be removed in a similar way.

\begin{figure}
    \begin{center}
    %\includegraphics[width=\columnwidth]{img/grammar.png}
    \includegraphics[width=\columnwidth]{img/parseresultsP.png}
    \caption{Simplified parse trees for the string "1+2*3;".}
    \label{fig:parseresultsP}
    \end{center}
\end{figure}

These trees are rather similar to the ones displayed before, and the
node naming conventions are the same. In this case, one of the parse
trees represents the \textit{+} symbol being processed first, while in
the other tree is the \textit{*} symbol that is processed first. To
solve this ambiguity, we use a simple algorithm: for any node in the
tree, if it matches the \textit{*} symbol, then the children nodes
must not match the \textit{+} symbol. If such nodes exist, they
represent a situation where the product happens before the sum, which
is not the desired behaviour.

\begin{lstlisting}
priority :: TFilter String
priority = neg anyChildrenMatches `when` matches "InfixExp2"
 where anyChildrenMatches = (matches "InfixExp1" $$). (concatMap getChildren) . getChildren 

disambiguationPrio :: TFilter String
disambiguationPrio = every priority
\end{lstlisting}

As in previous filters, the \textit{every} combinator is needed to
apply the filter to all the nodes in the tree, so that it discards the
tree if any of the nodes fail to satisfy it. Finally, it is possible
to generalize this filter, so as to allow easier reuse.

\begin{lstlisting}
before :: String -> String -> TFilter String
before x y = neg ((matches  x $$). (concatMap getChildren) . getChildren ) `when` matches  y

disambiguationPrioGeneric :: TFilter String
disambiguationPrioGeneric = every (before "InfixExp1" "InfixExp2") 
\end{lstlisting}

\subsubsection{Reject Filter}

Given a parser for the \textit{Filters} grammar,
and the simple input string \textit{"x = 
true"}, the output consists of two parse trees, as seen in figure
\ref{fig:parseresultsR}. By looking at them, it is possible to
understand that the parser can interpret the string \textit{true} as
either a boolean value, that is, a \textit{"Bool"} node, or an
identifier, that is, an \textit{"Id"} node. While both are technically
correct, this ambiguity needs to be eliminated by not allowing the use
of language keywords as language identifiers, thus the need for reject
filters.


\begin{figure}
    \begin{center}
    %\includegraphics[width=\columnwidth]{img/grammar.png}
    \includegraphics[width=\columnwidth]{img/parseresultsR.png}
    \caption{Simplified parse trees for the string "x = true;".}
    \label{fig:parseresultsR}
    \end{center}
\end{figure}

Since this parser is a scannerless parser, a string is actually a
sequence of tokens, where each token is a character. To recover the
string from the sequence of tokens, the \textit{implodeSubTree}
function is used. It is part of the tools available in the HaGLR
parser, and it converts a sequence of tokens into a string. The
resulting string is reversed, and thus the \textit{reverse} function
is used to fix the strings and enable adequate comparison between
them. 


According to the definition of this filter, when a node matches the
\textit{"Id"} production, then the string it derives into cannot match
any of the desired keywords. The following implementation only
considers the keywords \textit{"true"} and \textit{"false"} but it
trivially generalizes to more language keywords.


\begin{lstlisting}
reject :: TFilter String
reject = neg stringMatchesKeywords `when` matches "Id"
 where stringMatchesKeywords = (matches (reverse "true") `orElse` matches (reverse "false")) . head . getChildren . implodeSubTree


disambiguationRej :: TFilter String
disambiguationRej = every reject
\end{lstlisting}


As in previous disambiguation filter combinators, the \textit{every}
combinator is used to apply this filter to the whole tree. Of course,
only the \textit{"Id"} nodes can be affected by it, but they can be
located anywhere on the tree, hence why this combinator is needed. One
last step is to generalize this filter.

\begin{lstlisting}
reject :: String -> String -> TFilter String
reject    w p = neg (matches       (reverse w) . head . getChildren . implodeSubTree) `when` matches p

disambiguationRejGeneric :: TFilter String
disambiguationRejGeneric = every (reject "true" "Id" `o` reject "false" "Id")
\end{lstlisting}



\subsubsection{Follow Filter}

Given a parser for the \textit{Filters} grammar, and the simple input string \textit{"int x [] = [ 12 3 ]"}, the output consists of two parse trees, as seen in figure \ref{fig:parseresultsF}. Since whitespace is not directly specified in the grammar, if there are not any other separators, the parser cannot distinguish whether the string \textit{"12"} is one or two values. As such, the follow filters are used to solve this ambiguity. 
\begin{figure}
    \begin{center}
    %\includegraphics[width=\columnwidth]{img/grammar.png}
    \includegraphics[width=\columnwidth]{img/parseresultsF.png}
    \caption{Simplified parse trees for the string "int [] x = [ 12 3 ];".}
    \label{fig:parseresultsF}
    \end{center}
\end{figure}

In this situation, it is desired that the \textit{"Values1"}
production has the longest possible match in each of its children,
that is, tries to incorporate as many characters as possible into each
children. In this situation, it is enough to specify that either the
left child ends with a character that is not a number, or the right
child starts with a value that is not a number. This forces the output
to only contain this production when there are two values separated by
a whitespace in the input, because if there are no whitespaces, the
filter will reject the whole tree.

The implementation of this filter just specifies that, when the root
node is a \textit{"Values1"} node, then the first child's processed
string must end with a desired character or the second child's
processed string must start with one, so that between the two, there is
at least one whitespace. It is important to note that the parser
assembles whitespaces into various nodes automatically, and it is
taken advantage of this fact to describe this filter. 

\begin{lstlisting}
follow :: TFilter String
follow = (firstEndsGood `orElse` secondStartsGood) `when` matches "Values1"
 where firstEndsGood =    isOf (not . isDigit . head . getLastTerm . (!!0) . getChildren) 
       secondStartsGood = isOf (not . isDigit . head . getFirstTerm . (!!1) . getChildren) 

disambiguationFol :: TFilter String
disambiguationFol = every follow
\end{lstlisting}
As before, the \textit{every} combinator is used to apply this filter to the whole tree. Of course, only the \textit{"Values1"} nodes can be affected by it, but they can be located anywhere on the tree, hence why this combinator is needed. One last step is to generalize this filter. 

\begin{lstlisting}
follow :: String -> String -> TFilter String
follow    t r = (firstEndsGood `orElse` secondStartsGood) `when` matches t
 where firstEndsGood    = isOf $ flip notElem r . head . getLastTerm  . (!!0) . getChildren
       secondStartsGood = isOf $ flip notElem r . head . getFirstTerm . (!!1) . getChildren

disambiguationFolGeneric :: TFilter String
disambiguationFolGeneric = every (follow "Values1" "0123456789")
\end{lstlisting}

\subsubsection{Preference Filter}

Given a parser for the \textit{Filters} grammar, 
and the simple input string \textit{"if true
  then if false then 1 else 2"}, the output consists of two parse
trees, as seen in figure \ref{fig:parseresultsPref}. This is a rather
famous ambiguity problem generally described as the \textit{dangling
  else} problem. In this input string, there are two
\textit{if...then} clauses, and one \textit{else} keyword, but there
is no clue as to which \textit{if} statement does it belong
to. Therefore, the parser will generate two interpretations, in which
the \textit{else} keyword will associate with either of the
\textit{if...then} clauses.


\begin{figure}
    \begin{center}
    %\includegraphics[width=\columnwidth]{img/grammar.png}
    \includegraphics[width=\columnwidth]{img/parseresultsPref.png}
    \caption{Simplified parse trees for the string "if true then if false then 1 else 2;".}
    \label{fig:parseresultsPref}
    \end{center}
\end{figure}

In this situation, both interpretations are correct, but the developer
may prefer one over the other. This is where the preference filter
comes in. It will only act on ambiguities where several
interpretations are correct but there is one that is desired over the
others.

This filter, however, does not behave similarly to the other
filters. It works by comparing different parse trees, and then
choosing the best one, which is fundamentally different to the other
filters which operate on a single tree. Therefore, the implementation
of this filter is also very different, and is just a function which
operates on lists. This function compares each parse tree to every
other tree, and discards a parse tree if it is considered less
interesting than any other one. 


\begin{lstlisting}
preference :: String -> String -> [NTree String] -> [NTree String]
preference bad_token good_token l = preference' l l
 where preference' [] l = []
       preference' (x:xs) l = if any (x `isWorseThan` ) l then preference' xs l else x : preference' xs l
       isWorseThan (NTree x t) (NTree y tt) = if x == y then or (zipWith isWorseThan t tt) else x == bad_token && y == good_token

disambiguationPrefGeneric :: [NTree String] -> [NTree String]
disambiguationPrefGeneric = preference "Instr3" "Instr4"
\end{lstlisting}


\subsubsection{Arbitrary Filters}

Parser generators offer a predefined set of disambiguation rules. Such
rules are part of the language/notation used to define context free
grammars used by such generators to produce parsers. In such
traditional setting it is not possible to extend the predefined
disambiguation rules, since this would require a change in the parser
generator.

In our approach disambiguation rules are embedded in the grammar
description. As we presented in the previous sections, they can be
used to implement already known disambiguation rules. However, they
can also be used to implement new concepts and ideas that generally
are not possible to implement in the disambiguation rules of most
parser generators. This allows the developer to express any desired
disambiguation rule - specific to the language the developer is
defining - without the limitations of not being able to fine-tune the
filters.

As an example, in this section, it will be presented a filter that
associates any sum operations to the left, until an \textit{if} clause
is found, and inside the \textit{if} blocks, the sum operations will
associate to the right. While there is no immediate use for this
filter in our running example language, it is a good example of
different behaviour implemented into a filter.


\begin{lstlisting}
ff :: TFilter String
ff = iff (matches "Instr3") rightAssocEverything leftAssocUntil 
 where rightAssocEverything = every (right_assoc "InfixExp2")
       leftAssocUntil = isOf (all (satisfies ff) . getChildren) `o` left_assoc "InfixExp2"
\end{lstlisting}


The \textit{iff} combinator is fed three arguments, where the first is
just to check if the current node matches the \textit{if}
instruction. If so, the \textit{rightAssocEverything} portion of the
code is run, which just applies the \textit{right\_assoc} combinator
shown before to all the subtrees from that point onwards. If the
matching fails, as seen in the \textit{leftAssocUntil} portion of the
code, the \textit{left\_assoc} combinator is applied to the current
node, and the whole filter is recursively applied to all the
subtrees. 


\subsubsection{Embedded Filters}

To allow an easier usage of these disambiguation filters, an easier way to use the well-known filters was developed. A new data type was developed, where each possible constructor represents a disambiguation rule, and a developer can specify the filters using this data type, coupled with a conversion function that transforms it into a filter that would be produced by using the equivalent combinators. 

\begin{lstlisting}
data Filter t = RightAssoc t 
              | LeftAssoc t
              | Before [t] [t] 
              | After [t] [t]
              | NotFollowedBy t t
              | Reject t t
              | Reject_List [t] t
\end{lstlisting}
This approach is easier to use, as there is a helper function being used to generate the filters that finds the name of the production nodes given a symbol used in it. This implies that the developer does not need to know the names of the productions to write the filters, and can use the symbols instead, which is generally more intuitive. However, the helper function that does this convertion is computationally intense, and so this approach is less efficient than the pure combinator approach. It is possible, however, to combine this approach with the combinator approach, allowing a developer to write the most trivial filters with this approach, and to write the most complex filters using combinators. 

The following filter is an implementation of most of the filters previously described, combined into one filter, using this strategy. The priority filter is changed into a filter that defined priority between the modulus and equality operations, as these filters will be needed in the following section. This change is trivial using this strategy, as there is no need to know the production names, only the symbols that are relevant. The preference filter is not included as it cannot be described using this strategy. 


\begin{lstlisting}
filters :: TFilter String
filters = gen_filters [
    "Values1" `NotFollowedBy` "0123456789", 
    Reject_List ["true", "false", "break"] "Id",
    LeftAssoc "+",
    ["=="] `Before` ["%"]
    ] 
disambiguation :: TFilter String
disambiguation = every filters
\end{lstlisting}
To further improve developer experience, some filters were extended so as to enable the processing of a list of arguments instead of a single argument. In practice, as an example, this means that the developer does not need to write various reject filters for different keywords, and can just opt to write them all in a list. This approach was taken to further improve the usage of the reject and priority filters, as well as the \textit{matches} combinator. As such, the following propositions are true.

\begin{lstlisting}
matches x `orElse` matches y==matchesL [x,y]
reject k x `o` reject l x==rejectL [k,l] x
before a b `o` before a c==beforeL [a] [b,c]
\end{lstlisting}
