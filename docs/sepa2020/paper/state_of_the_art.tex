\section{State of the Art}
\label{chapter:sota}
There are various different parsing algorithms proposed to solve the parsing problem. The earliest solution to be used was to embed the grammar in the code, where there was no separation between the grammar and the rest of the code. This was a solution that made it very difficult to change the grammar once it was coded in, since it was mixed with the code.   

\subsection{BNF Notation}
The BNF notation is a notation for specifying context-free grammar, generally used for specifying the exact grammar of programming languages. It was proposed by \cite{conf/ifip/Backus59}, to describe the language of what became known as ALGOL 59. 

In the example in listing \ref{grammar}, the BNF notation is used to describe the grammar of arithmetic expressions.
%
This grammar is inherently recursive: an expression (Exp) can be defined as an expression (Exp), an arithmetic sign ('+') and another expression (Exp). The entirety of a grammar is expressed in this way, which is simple to understand and powerful when compared to embedding the grammar in the code. 

The BNF notation is extremely interesting as it laid the foundation to having the grammar separated from the code, such that it would be easy to change the grammar without having to change any of the remaining code. 

However, this notation can be ambiguous. A concrete instance of text can be interpreted in different ways, all of them correct according to the specification. As an example, the text \textit{1+2+3} can be considered. There are two possible ways to interpret this text according to the specification, which represent different order of operations, that is, whether the left sum or the right product is processed first. 

%\begin{lstlisting}[caption=Two interpretations of \textit{1+2+3}, captionpos=t]
%1. Exp      
%      => Exp '+' Exp 
%      => Exp '+' ( Exp '+' Exp ) 
%      => int '+' ( int '+' int )     
%2. Exp 
%      => Exp '+' Exp 
%      => ( Exp '+' Exp ) '+' Exp 
%      => ( int '+' int ) '+' int     
%\end{lstlisting}
%It is expected to have some ambiguity in most grammars, as natural language and the way of thinking of a human being is generally ambiguous. Having ambiguity as a possibility in a grammar lends it more expressivity but also more difficulties in developing a tool to recognize said grammar, as the end result produced by this tool might need to be unambiguous. 

%\subsection{Abstract Syntax Trees}

%A parser typically constructs the parsing result as a syntax tree. This is a tree that contains all the information from the input categorized in accordance to the grammar. %Example needed
%As an example, according to listing~\ref{grammar}, the string "1+2" is parsed with no ambiguity and an Abstract Syntax Tree is produced. However, the string "1+2+3" is ambiguous, as the parser does not understand operation priority, and therefore will produce two outputs, as seen in figure~\ref{fig:parseresultsA}. For a generalized parser, the output is not a tree but a \textbf{\textit{forest}}, that is, a list of trees. 

%%As an example, according to the grammar seen in listing~\ref{grammar}, for an input "1+2", the output can be seen in figure \ref{fig:SyntaxTree1}. However, for an input "1+2*3", and with no disambiguation rules defined, the parser does not understand arythmetic priority and can generate two possible outputs, as seen in figure \ref{fig:SyntaxTree2}. For a generalized parser, both possibilities are generated, and therefore the output is not a tree but a \textbf{\textit{forest}}, that is, a list of trees. 
%
%Since, in this work, the disambiguation is to be performed after the parsing step, the disambiguation rules should be applied to the syntax forest, trying to find undesired patterns in them and removing them if any of these patterns are found. Such process will be described in more detail in the following sections.

\subsection{Common Parsers}
While it is common for programming languages to be expressed as grammars, for example using the BNF notation, there are various ways to generate a parser given such specification. Each alternative method has its own advantages and disadvantages. The first to be relevant were the most powerful in terms of compilation and execution time. As such, the grammars were changed to best fit the method: the developer had to both focus on writing the correct grammar and adapting it to fit the parser generator. The previous example uses left-recursion, which is impossible to parse \cite{Aho:2006:CPT:1177220} for some of these algorithms. Therefore, the previous example would have to be changed so as to not have left-recursion, before it could be used in some parser generators. 

One of the most well-known parser generators, YACC, is a LALR parser generator \cite{Johnson79yacc:yet}: this relies on a lightweight algorithm which was perfect for the time it was developed, that is, 1975, when it was much more necessary to restrict program runtime and memory size. 

Another example of a popular parser generator is ANTLR \cite{Parr:2011:LFA:1993498.1993548}, whose development started in 1989 as a LL(*) parser generator. The LL(*) algorithm allows for parsing decisions to be taken by looking at the following tokens in the input stream. ANTLR 4 uses the ALL(*)\cite{parr2014adaptive}, which is \textit{O(N\textsuperscript{4})} in theory but is shown to consistently perform linearly in practice.  

While Yacc generates C code and ANTLR generates code for various programming languages, one of the most popular parser generators for Haskell is Happy \cite{happy}, which enables the developer to supply a file with the specification of a grammar, and in turn generates a parser, that is, a module of code that can read text according to that grammar's specifications. Happy is part of the Haskell Platform, being one of the most famous Haskell parsing tools. Due to its rather big popularity and regular maintenance, it is a fairly well optimized tool.

\subsection{Generalized Parsing}

Several parsing techniques do not deal with ambiguity properly. The input is expected to be unambiguous, and when it is not, a certain interpretation of such ambiguity is chosen so as to continue parsing. This results in runtime-wise efficient but not so expressive parsers, as they ignore any ambiguity problems that could arise.

Ambiguity can be dealth with using GLR parsers, which are slower than their non-generalized counterparts, due to their additional flexibility in dealing with non-determinism: when faced with an input with several different possible outputs, a GLR parser \cite{Tomita:1985:EPN:537456} will produce all of the outputs instead of selecting one of them. If no non-determinism is present, a GLR parser will behave just like a LR parser \cite{GPsomecosts}, which is efficient. With the constant advances in technology, the limitations that made this technique undesirable are gone and there are parser generators that allow the use of the GLR algorithm, such as Happy. 

However, GLR is not the only generalized algorithm. The GLL algorithm \cite{scott2010gll,scott2013gll} is also generalized, but much less explored. 
%While there aren't any powerful GLL parser generators on the rise in the industry, it is definitely an interesting idea. 
This algorithm is worst-case cubic in both time and space and there are possible optimizations to it~\cite{Afroozeh2015}. 

\subsection{Scannerless Parsing}

Generally speaking, parser applications are divided in two components: the lexer and the parser. The lexer takes the input and breaks it into a list of tokens~\cite{Saraiva02b}, and then the parser takes those tokens and matches them with the production rules to produce the actual parsing result. 

Scannerless parsing \cite{Salomon:1989:SNP:73141.74833} consists of skipping the lexer entirely and treating each character from the input as a token, which is fed directly into the parser. Scannerless parsing removes the necessity of describing the tokens in the grammar specification, allowing the developer to write the grammar without worrying as much with comforming with the parser technology. Scannerless parsers are compositional, therefore allowing for two parsers to be merged without needing to change them. 

\subsection{Disambiguation Filters for Scannerless Generalized Parsing}
\label{sec2.5}

In this work, the focus is on scannerless generalized parsers. For such, as they deal with ambiguous inputs, it is expected to get a list of outputs as a result, which represent all possible interpretations. However, not all possible interpretations are desired: depending on the situation, a developer might want to only get one or a small subset of parse trees, instead of all the possibilities. 

The task of processing the list of the ambiguous parse trees produced by a parser and removing the undesired is called disambiguation. Typically, such filtering is done on the parser, that is, modifying part of the parser so that the undesired interpretations cannot be produced. Some new rules for disambiguation are needed when dealing with scannerless parsing. In this section, some filters for disambiguation are presented and described, according to the work of van den Brand et al.\cite{vandenBrand2002}. 
%\subsubsection{Priority and Associativity Filter}

%The \textbf{priority} and \textbf{associativity} filters are the most commonly known out of the disambiguation filters. 
The  \textbf{priority} filter specifies that certain productions have a higher priority than others, while the \textbf{associativity} filter specifies that an operator associates left or right. %As such, extending the grammar of arithmetic expressions seen in listing~\ref{grammar}, these filters could be described as shown in listing \ref{grammarPA}.
%
%\begin{lstlisting}[label=grammarPA, caption={Grammar of arithmetic expressions, with priority and associativity filters}, captionpos=t]
%Exp :         Exp '+' Exp   %left
%            | Exp '-' Exp   %left
%            | Exp '*' Exp   %left
%            | Exp '/' Exp   %left
%            | '(' Exp ')' 
%            | int             
%
%{Exp : Exp '*' Exp} > {Exp : Exp '+' Exp}
%\end{lstlisting}
%\subsubsection{Reject Filter}

The \textbf{reject} filter enables the creation of keywords in the grammar. In other words, it rejects some productions from deriving into certain sequences. 
%This is extremely useful as in most programming languages, some keywords cannot be used as variable names, and 
This filter allows for a clean implementation of reserved keywords. For example, in the C programming language, it shouldn't be allowed for a variable to be named "while", as that is a reserved keyword used in defining loops. 
%A developer could specify such filters by describing what the production should not derive into, as shown in listing \ref{grammarRej}.
%
%\begin{lstlisting}[label=grammarRej, caption={Reject filter for "while" reserved keyword}, captionpos=t]
%Identifier : "while" {reject}
%\end{lstlisting}
%\subsubsection{Follow Filter}

The \textbf{follow} filter solves
an ambiguity that arises in scannerless parsing. When the
grammar dictates that a sequence of symbols can be parsed using one
single production or a sequence of productions, for example, a
sequence of digits which could be read as a single number or several
numbers with no separators, the follow filter specifies that the
longest match is to be performed.
%filter solves a less obvious ambiguity that arises in scannerless parsing. Let us consider as example the grammar specified in listing \ref{grammarFollow}, which specifies a list of values, separated by whitespaces, in which a whitespace is defined as zero or more whitespace characters such as spaces and tabs. This example can be used to define the grammar for vector declarations of Matlab, where a vector can be declared as a list of values using only whitespace as separators. 
%\begin{lstlisting}[label=grammarFollow, caption={Grammar for a list of values}, captionpos=t]
%Values :     Values Ws Values
%           | Value
%
%Ws : [ \\n\\t]*
%Value : [0-9]+
%\end{lstlisting}
%When the input is a single number with various digits, such as "37", it is ambiguous. It can be interpreted as a single "Value", or as two separated "Values", separated by exactly zero "Ws". The second interpretation is generally unwanted, and a way to fix this would be to change the definition of "Ws" to only allow for one or more whitespaces. However, when "Ws" is the default whitespace definition used throughout the grammar, this change is undesired as it would force the grammar to be changed to accomodate this change. 
%
%Hence, in this case, the follow filter (also known as longest match filter) is defined to specify that a "Value" cannot be followed by a digit. This way, "Value" must contain all the digits before an actual whitespace occurs, therefore forcing a longest match to occur. For the example input "37", the second interpretation where two separated "Values" contain each one digit would be invalid, as the first "Values" string must be followed by a non-digit character, but is followed by the "7" digit. 
%
%\begin{lstlisting}[label=filterRej, caption={Follow filter for the grammar for a list of values}, captionpos=t]
%Value -/- [0-9]
%\end{lstlisting}
%\subsubsection{Preference Filter}

When there are several correct interpretations of a given input but some are preferred over others, a \textbf{preference} filter is used. It specifies which parse results should be removed when there are several correct outputs but the developer wants to select only a part of them.
This filter is the go-to filter to remove the dangling else problem, occurs when there are two \textit{if} clauses and only one \textit{else} clause, and thus it is not clear to which \textit{if} clause the \textit{else} clause associates to. 
%which can be exemplified by the input \textit{if bool1 then if bool2 then out1 else out2} that can be interpreted in two ways,\textit{if bool1 then (if bool2 then out1 else out2)} or \textit{if bool1 then (if bool2 then out1) else out2}. 

%In the listing \ref{grammarPref}, the dangling else problem is exemplified, with the preference filter already described. The ambiguity present here can be exemplified by the input \textit{if bool1 then if bool2 then out1 else out2}, which can be interpreted in two ways,\textit{if bool1 then (if bool2 then out1 else out2)} or \textit{if bool1 then (if bool2 then out1) else out2}. 
%
%\begin{lstlisting}[label=grammarPref, caption={Grammar exemplifying the dangling else problem, and usage of preference filter}, captionpos=t]
%Term :  "if" Bool "then" Term %prefer
% | "if" Bool "then" Term "else" Term
%\end{lstlisting}
\subsection{Haskell XML Toolbox and HaGLR}
Syntax trees are generalized trees which can represent a program. Generalized trees are often called Rose Trees in the functional programming setting and are well studied in several contexts. One of them is XML, for which there are several generic tools that can be used. 
In this work, the filter variant of the Haskell XML Toolbox \cite{hxt} is used as a base for building combinators for filtering syntax trees. 
%In this library, Rose Trees are known as \textit{NTree}, and these trees and some tools for manipulating them are defined in the module \textit{Data.Tree.NTree.TypeDefs}. The building blocks used in this work are defined in the module \textit{Data.Tree.NTree.Filter}. 
%
%
%\subsection{HaGLR Parser Generator}
 %which conflict with performance ones. In fact, it is currently extremely inefficient, as shown in Fernandes et al.'s work, where AG.bib, a large bibliographic database of attribute grammars related literature (compiled and available at INRIA) was fed as input to both a Happy parser and a HaGLR parser, and the Happy parser was much more efficient. 
The HaGLR tool \cite{haskellglrgood} is a Haskell implementation of a GLR parser generator, which was implemented with pedagogic purposes. 
%Since performance is not our main focus and HaGLR is a generalized parser generator, it is adequate for this work. 
It produces as result a pure parse tree forest, which is a list of parse trees. This is not the case for all generalized parsers as some optimizations change the representation of the parse forests to save memory, which use a different, more compact approach, but are less intuitive to work around. 