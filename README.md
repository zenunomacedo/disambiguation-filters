# README #

This repository contains the code and documentation for disambiguation filters, expressed as Haskell combinators.  

## TODO: ##

* Write paper for SLE-conf 2019 (deadline SOON)
* Upload actual source code
* Build .cabal file
* Finish README.md
* More examples


### How do I get set up? ###

To install dependencies, just use cabal to resolve them through the command:

`cabal install`

Everything should be resolved automatically and you're good to go. Imply import the libraries into your code and start building filters. 


### Contact owner ###

Email: jose.n.macedo AT inesctec.pt