module Main where

import Benchmark.Small.Ambiguous.Parser as AP
import Benchmark.Small.Unambiguous.Parser as UP

import Language.HaGLR.RegExpAsCfg (implodeSubTree)

import Data.Tree.NTree.Filter
import Data.Tree.Class

import Filters.Generics
import Filters.Embed
import Filters.Filters

import Criterion.Main
import Test.QuickCheck
import Test.QuickCheck.Gen

main = benchSmall

extensiveTests = defaultMain [
  bgroup "Small" [ bench "Ambiguous"    $ nfIO (testAmbig   "Input/small.in")
                 , bench "Unambiguous"  $ nfIO (testUnambig "Input/small.in")
               ] ,
  bgroup "Medium" [ bench "Ambiguous"   $ nfIO (testAmbig   "Input/medium.in")
                 , bench "Unambiguous"  $ nfIO (testUnambig "Input/medium.in")
               ] ,
  bgroup "Large" [ bench "Ambiguous"    $ nfIO (testAmbig   "Input/large.in")
                 , bench "Unambiguous"  $ nfIO (testUnambig "Input/large.in")
               ] ,
  bgroup "Huge" [ bench "Ambiguous"     $ nfIO (testAmbig   "Input/huge.in")
                 , bench "Unambiguous"  $ nfIO (testUnambig "Input/huge.in")
               ] 
  ]

benchSmall = defaultMain [
  bgroup "Small" [ bench "With Filters"    $ nfIO (testAmbig "Expression.txt")
                 , bench "Without Filters"  $ nfIO (testAmbigNoFilters "Expression.txt")
               ]
  ]

oldMain = defaultMain [
  bgroup "Small" [ bench "Ambiguous"    $ nfIO (testAmbig "Expression.txt")
                 , bench "Unambiguous"  $ nfIO (testUnambig "Expression.txt")
               ]
  ]

--Tests for Criterion
testAmbig file = do 
    s <- readFile ("Benchmark/Small/" ++ file)
    let work = disambiguation_small $ AP.glr_parser_aterm s
    return (length $ work, sum $ map cardTree work)

testAmbigNoFilters file = do 
    s <- readFile ("Benchmark/Small/" ++ file)
    let work = AP.glr_parser_aterm s
    return (length $ work, sum $ map cardTree work)

testUnambig file = do 
    s <- readFile ("Benchmark/Small/" ++ file)
    let work = UP.glr_parser_aterm s
    return (length $ work, sum $ map cardTree work)



--QuickCheck
testSoundness :: IO()
testSoundness = quickCheck prop_valid

genExpr :: Gen String
genExpr = do
  let genV1p = fmap show $ choose (0::Int , 1000)
      genV2p = fmap show $ choose (0::Int , 1000)
      genV1n = fmap (\k -> "(" ++ show k ++ ")") $ choose (-1000::Int, 0)
      genV2n = fmap (\k -> "(" ++ show k ++ ")") $ choose (-1000::Int, 0)
  op <- elements ["+", "*", "-", "/"]
  expr1 <- frequency [(1, genExpr), (3, genV1p), (1, genV1n)]
  expr2 <- frequency [(1, genExpr), (3, genV2p), (1, genV2n)]
  return (expr1 ++ op ++ expr2)

prop_valid :: String -> Property
prop_valid x = forAll genExpr $ \e ->  --using generator
                within 3000000 $       --max 1 second per test
                let resultAmbiguous = disambiguation_small $ AP.glr_parser_aterm e 
                    resultUnambiguous = UP.glr_parser_aterm e
                in
                singletonList resultAmbiguous && 
                singletonList resultUnambiguous &&
                eval (head $ resultAmbiguous) == eval (head $ resultUnambiguous)

singletonList l = length l == 1

--mudar isto
{-
prop_equal_tiger :: String -> Property
prop_equal_tiger x_tiger =  
                let resultAmbiguous = disambiguation_small $ AP.glr_parser_aterm e 
                    resultUnambiguous = UP.glr_parser_aterm e
                in
                (length (resultAmbiguous)   == 1) && 
                (length (resultUnambiguous) == 1) &&
                pp_tiger (head $ resultAmbiguous) == pp_tiger (head $ resultUnambiguous)
-}


--Expression evaluator for parse trees
eval :: NTree String -> Int
eval (NTree _ ((NTree ('n':'t':_) _):a:(NTree ('n':'t':_) _):_)) = eval a --handle parenthesis
eval (NTree _ (a:(NTree ('n':'t':_) l):b:_)) = (eval a) `op` (eval b) --handle infix op's
 where op = case (getNode . head . getChildren . head $ l) of 
             "+" -> (+)
             "*" -> (*)
             "/" -> div
             "-" -> (-)
eval x@(NTree ('V':'a':'l':_) l) =  read $ dirtyFix $ filter (\x -> not( x `elem` "Ws")) $ getNode $ head $ getChildren $ implodeSubTree x
 where dirtyFix ('-':xs) = '-':reverse xs
       dirtyFix xs = reverse xs
eval (NTree s l) = eval (head l)


-------------- DEFINITION OF FILTERS FOR AMBIGUOUS GRAMMAR
--associativity
filterPlus = neg ((matches "InfixExp4" `orElse` matches "InfixExp5") . head . getChildren . (!!2) . getChildren) `when` (matches "InfixExp4" `orElse` matches "InfixExp5")
filterMult = neg ((matches "InfixExp3" `orElse` matches "InfixExp6") . head . getChildren . (!!2) . getChildren) `when` (matches "InfixExp3" `orElse` matches "InfixExp6")
--priority
filterPlusSub_MultDiv = neg (((matches "InfixExp4" `orElse` matches "InfixExp5") $$). (concatMap getChildren) . getChildren ) `when` (matches "InfixExp3" `orElse` matches "InfixExp6")

disambiguation_small = (every (filterPlus `o` filterMult `o` filterPlusSub_MultDiv) $$) 




--Alternative, using Embed
disambiguation_small' :: [NTree String] -> [NTree String] 
disambiguation_small' = (every (gen_filters AP.grammar filters) $$)
 where filters = [
                   -- LeftAssocL ["+", "-"] --Estouram pq tenta associar o - de numero negativo
                   LeftAssocL ["*", "/"]
                   ,["+","-"] `After` ["*","/"]
                   ]

--Alternative, using pre-defined filters (different from Embed because it ignores the "-" sign from negative numbers, which is what makes Embed fail)
disambiguation_small'' :: [NTree String] -> [NTree String]
disambiguation_small'' = (every (filterPlus'' `o` filterMult'' `o` filterPlusSub_MultDiv'') $$)
 where filterPlus'' = left_assocL ["InfixExp4", "InfixExp5"]
       filterMult'' = left_assocL ["InfixExp3", "InfixExp6"]
       filterPlusSub_MultDiv'' = ["InfixExp4", "InfixExp5"] `before_l` ["InfixExp3", "InfixExp6"]
