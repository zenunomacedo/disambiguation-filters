module Main where

import Benchmark.Tiger.Ambiguous.Parser as AP
--import Benchmark.Tiger.Unambiguous.Parser as UP

import Language.HaGLR.RegExpAsCfg (implodeSubTree)

import Data.Tree.NTree.Filter
import Data.Tree.Class

import Filters.Generics
import Filters.Embed
import Filters.Filters

import Criterion.Main
import Test.QuickCheck
import Test.QuickCheck.Gen

main = defaultMain [
  bgroup "8queen" [ bench "Ambiguous"    $ nfIO (testAmbig   "Input/8queen.tiger")
                  , bench "Unambiguous"  $ nfIO (testUnambig "Input/8queen.tiger")
               ]
  ]

--Tests for Criterion
testAmbig file = do 
    s <- readFile ("Benchmark/Tiger/" ++ file)
    let work = disambiguation_tiger $ AP.glr_parser_aterm s
    return (length $ work, sum $ map cardTree work)

testUnambig file = do 
    s <- readFile ("Benchmark/Tiger/" ++ file)
    let work = undefined :: [NTree String]--UP.glr_parser_aterm s
    return (length $ work, sum $ map cardTree work)



--QuickCheck
testSoundness :: IO()
testSoundness = quickCheck prop_equal_tiger

prop_equal_tiger :: String -> Bool
prop_equal_tiger e = let 
                    resultAmbiguous   = disambiguation_tiger $ AP.glr_parser_aterm e 
                    resultUnambiguous = undefined :: [NTree String]--UP.glr_parser_aterm e
                in
                (length (resultAmbiguous)   == 1) &&
                (length (resultUnambiguous) == 1) &&
                pp_tiger (head $ resultAmbiguous) == pp_tiger (head $ resultUnambiguous)


--Expression evaluator for parse trees
pp_tiger :: NTree String -> String
pp_tiger (NTree x []) = x
pp_tiger (NTree x l) = concatMap pp_tiger l



-------------- DEFINITION OF FILTERS FOR AMBIGUOUS GRAMMAR
disambiguation_tiger :: [NTree String] -> [NTree String] 
disambiguation_tiger = (every (gen_filters AP.grammar filters) $$)
 where filters = [
                    LeftAssoc "&" 
                    , ["+"] `Before` ["-"]
                    , ["&"] `After` ["=", ">", "<", ">=", "<="]
                   ]