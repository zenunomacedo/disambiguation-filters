
-----------------------------------------------------------------------------
-- |
-- Copyright   :  (c) Universidade do Minho, 2016
-- License     :  LGPL
--
-- Maintainer  :  João Saraiva - jas@di.uminho.pt
-- Stability   :  experimental
-- Portability :  portable
--
-- Representing Regular Expressions as Grammars
--
-----------------------------------------------------------------------------

module Language.HaGLR.RegExpAsCfg
       ( regExpAsCfg
       , regExpAsCfg'
       , regExpAsCfg''
       , accept_reAsCfg
       , implodeCfgForest
       , implodeSubTree
       , implodeSubTree'
       , implodeAst
       , implodeAstSymb
       ) where

import Data.List
import Language.HaLex.RegExp
import Language.HaLex.RegExp2Fa
import Language.HaLex.Dfa
import Language.HaLex.Minimize
import Language.HaLex.RegExpParser

import Language.HaGLR.Cfg
import Language.HaGLR.Semantics
import Language.HaGLR.LR_Semantics
import Language.HaGLR.G_LR_Semantics

import Data.Tree.NTree.TypeDefs

import Text.Show.Pretty

-- | Regular Expressions to Grammars, directly
--   (to be finished)


regExpAsCfg' :: RegExp Char -> ProdName -> Cfg Char String
regExpAsCfg' re pn = prods2cfg (regExpAsCfg'' re pn 0) (pn ++ "0")

regExpAsCfg'' :: RegExp Char -> String -> Int -> [Prod Char String]
regExpAsCfg'' Epsilon      pn n =  [(pn, [NT (pn ++ show n)])]
regExpAsCfg'' (Literal c)  pn n =  [(pn, [NT (pn ++ show n) , T c])]
regExpAsCfg'' (Or re1 re2) pn n =  (regExpAsCfg'' re1 pn n) ++
                                   (regExpAsCfg'' re2 pn n)
regExpAsCfg'' (Then re1 re2) pn n =
                  map (\(n,prds) -> (n,prds++[p2Name])) p1 ++ p2
  where p1 = regExpAsCfg'' re1 pn n
        p2 = regExpAsCfg'' re2 pn (n+1)
        (_ , p2Name:_) = head p2

regExpAsCfg'' (OneOrMore re) pn n =
  let me   = NT (pn ++ show n)
      next = NT (pn ++ show (n+1))
      expanded = regExpAsCfg'' re pn (n+1)
  in  [ (pn, [me, me, next] )  -- Id1 -> Id2 Id1  --left recursive is better
      , (pn, [me, next] )]     -- Id1 -> Id2
      ++ expanded
regExpAsCfg'' (Star re) pn n =
  let me   = NT (pn ++ show n)
      next = NT (pn ++ show (n+1))
      expanded = regExpAsCfg'' re pn (n+1)
  in  [ (pn, [me, me, next] )  -- Id1 -> Id2 Id1  --left recursive is better
      , (pn, [me] )]           -- Id1 -> Id2
      ++ expanded
regExpAsCfg'' (RESet cs) pn n =
  concatMap (\reg -> regExpAsCfg'' reg pn n) (map Literal cs)


{- |
Regular Expressions as Grammars via Finite Automata:

- Convert a RegExp into a minimized Dfa
- use states as nonterminal symbols
- use language as set of terminal symbols
- add a transition p -> aq for any transition p -> q on letter a in the original automaton
- add a epsilon production st ->  for all Dfa final states
- use initial state as initial symbol in the grammar
-}


regExpAsCfg :: Ord sy => RegExp sy -> ProdName -> Cfg sy Int
regExpAsCfg re id = cfg
  where dfa = beautifyDfa  $ stdMinimizeDfa $ regExp2Dfa re
        (Dfa v _ s z _) = dfa

        tt :: [(Int,[Int])]
        tt  = dfa2tdfa dfa

--   ttID :: [(Int,sy,Int)]
        ttID = concat $ map (\(st,sts) -> zipWith (\a b -> (st,b,a)) sts v) tt

        tt2cfg = map (\(f,s,d) -> (id , [ NT f
                                        , T s
                                        , NT d
                                        ])) ttID
        z2cfg = map (\ st -> ("" , [NT st])) z    -- ?? epsilon -> no name
                                                  -- needed when imploding
        cfg = prods2cfg (tt2cfg `union` z2cfg) s


-- regExpAsCfg'' :: Ord sy => RegExp sy -> ProdName -> [Prod Char String]
regExpAsCfg_dfa re id = tt2cfg `union` z2cfg
  where dfa = stdMinimizeDfa . regExp2Dfa $ re
        (Dfa v _ s z _) = dfa
        -- tt :: [(Int,[Int])]
        tt  = dfa2tdfa dfa
        ttID = concat $ map (\(st,sts) -> zipWith (\a b -> (st,b,a)) sts v) tt

        tt2cfg = map (\(f,s,d) -> (id, [ NT (show f)
                                            , T ((s))
                                            , NT (show d)
                                            ])) ttID
        z2cfg = map (\ st -> ("" , [NT (show st)])) z


accept_reAsCfg :: (Ord sy, Show sy)
                => RegExp sy -> [sy] -> ProdName -> [NTree String]--[(Bool, NTree String)]
accept_reAsCfg re inp id = let  cfg = regExpAsCfg re id
                           in   glr_aterm cfg inp



implodeCfgAst :: RegExp Char -> [Char] -> NTree String
implodeCfgAst re inp = let rt = head $ accept_reAsCfg re inp "Re"
                       in implodeSubTree  rt

implodeSubTree' :: ProdName -> NTree String -> NTree String
implodeSubTree' re rt = stopTD_NTree implodeSubTree (==re) rt


implodeCfgForest :: RegExp Char -> [Char] -> [NTree String]
implodeCfgForest re inp = let forest = accept_reAsCfg re inp "Re"
                          in map implodeSubTree forest



-- | Implodes a parse forest produced by a scannerless parser
--   The names of regulares expressions are the ProdNames of the
--   equivalent (regular) grammar. The implosing reconstructs the lists
--   of chars (that match the original regular expression) from the parse tree.
--   It gets as input the parse forest, the list of regular expression names
--   It returns the new parse forest.

implodeAst :: [NTree String] -> [ProdName] -> [NTree String]
implodeAst pf res = map (implodeSubTrees res) pf

implodeSubTrees :: [ProdName] -> NTree String -> NTree String
implodeSubTrees res rt = stopTD_NTree implodeSubTree (`elem` res) rt

implodeSubTree :: NTree String -> NTree String
implodeSubTree rt@(NTree v sf) | leaves == v = NTree v []
                               | otherwise   = NTree v [NTree leaves []]
  where leaves = concat $ leavesPerLevel rt
--               NTree v [NTree (concat $ leavesPerLevel rt) []]


implodeAstSymb :: [NTree (Symb Char String)] -> [ProdName]
               -> [NTree (Symb String String)]
implodeAstSymb pf res = flip implodeAstSymb' res $ map (mapNTree chrToStr) pf
  where
    implodeAstSymb' :: [NTree (Symb String String)] -> [ProdName] -> [NTree (Symb String String)]
    implodeAstSymb' pf res = map (implodeSubTreesSymb res) pf

    chrToStr :: Symb Char String -> Symb String String
    chrToStr (NT nt) = NT nt
    chrToStr (T c)   = T [c]



-- res means restriction
implodeSubTreesSymb :: [ProdName] -> NTree (Symb String String) -> NTree (Symb String String)
implodeSubTreesSymb res rt = stopTD_NTree implodeSubTreeSymb p rt
  where p (T _) = False
        p (NT nt) = nt `elem` res
-- stopDT_NTree implodeSubTreeSymb (p . rootLabel) rt


implodeSubTreeSymb :: NTree (Symb String String) -> NTree (Symb String String)
implodeSubTreeSymb rt@(NTree v sf) | leaves == v = NTree v []
                                   | otherwise   = NTree v [NTree leaves []]
  where leaves = T (collectT (leavesPerLevel rt))
        collectT :: [Symb String String] -> String
        collectT [] = ""
        collectT (T x : xs) = x ++ collectT xs
        collectT (NT _ : xs) = collectT xs


-- implodeAstSymb :: [(Bool,NTree (Symb Char String))] -> [ProdName]
--                -> [(Bool,NTree (Symb String String))]
-- implodeAstSymb pf prods =
--   let reshaped = map (\(b, rt) -> (b, mapNTree chrToStr rt)) pf
--   in  implodeAstSymb2 reshaped prods
--   where
--     chrToStr :: Symb Char String -> Symb String String
--     chrToStr (NT nt) = NT nt
--     chrToStr (T c)   = T [c]

-- implodeAstSymb2 :: [(Bool,NTree (Symb String String))] -> [ProdName]
--                -> [(Bool,NTree (Symb String String))]
-- implodeAstSymb2 pf [] = pf
-- implodeAstSymb2 pf (prod:prods) =
--   let aaa = implodeAstSymb' pf prod
--   in  implodeAstSymb2 aaa prods
--   where
--     implodeAstSymb' :: [(Bool,NTree (Symb String String))] -> ProdName -> [(Bool,NTree (Symb String String))]
--     implodeAstSymb' pf prod = map (\(b,rt) -> (b, implodeSubTreesSymb prod rt)) pf


-- implodeSubTreesSymb :: ProdName -> NTree (Symb String String) -> NTree (Symb String String)
-- implodeSubTreesSymb prod rt = stopTD_NTree implodeSubTreeSymb (p) rt
--   where p (T _) = False
--         p (NT nt) = nt == prod