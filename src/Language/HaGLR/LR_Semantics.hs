-----------------------------------------------------------------------------
-- |
-- Copyright   :  (c) Universidade do Minho, 2004
-- License     :  LGPL
--
-- Maintainer  :  João Fernandes
-- Stability   :  experimental
-- Portability :  portable
--
-- Generalised LR parsing + construction of a XML and, or ATerm AST
--
-----------------------------------------------------------------------------

module   Language.HaGLR.LR_Semantics
  (
--        lr_ast_xml
--        , lr_ast_xml'
--        , lr_ast_xmldot2file
   lr_ast_aterm
  , lr_sem
--  , xml
  , aterm
  ) where

-----------------------------------------------------------------------------

import Language.HaLex.Dfa
import Language.HaGLR.Stack
import Language.HaGLR.Cfg
import Language.HaGLR.LR_0
import Language.HaGLR.SLR
import Language.HaGLR.Semantics

import Data.Tree.NTree.TypeDefs
-- import LALR

-- import Text.XML.HaXml.Types
-- import PrettyPrint.XML.Xml2Dot
-- import PrettyPrint.XML.Dot

-----------------------------------------------------------------------------

lr_ast_aterm :: (Ord t,Ord nt,Show t,Show nt)
             => Cfg t nt -> [t] -> (Bool, NTree String)
lr_ast_aterm = semantics_lraccept aterm


semantics_lraccept :: (Ord t, Ord nt) =>
     (Bool -> ([(String, [Symb t nt])], Cfg t nt) -> r)
     -> Cfg t nt -> [t] -> r

semantics_lraccept f g inp =  lr_sem f semantics lookupTT_g lookupAT_g [s] inp
  where eg@(Cfg t nt r p)   = expand_cfg g      -- The expanded grammar
        dfa@(Dfa v q s z d) = ecfg2LR_0Dfa eg   -- The LR_0 DFA

        at                  = e_slr_at  eg      -- The Action Table

        lookupAT_g          = lookupAT t at     -- The lookup fun. for the AT
        lookupTT_g          = d                 -- The lookup fun. for the TT
        semantics           = ([],g)

lr_sem :: (Eq t, Eq nt) =>
        (Bool -> ([(String, [Symb t nt])], Cfg t nt) -> r)
     ->          ([(String, [Symb t nt])], Cfg t nt)
     -> (st -> Symb t nt -> st)
     -> (st -> Symb t1 nt -> Action st [Symb t nt])
     -> [st]
     -> [t1]
     -> r

lr_sem f semantics l_tt l_at sk [] = lr' f semantics l_tt l_at sk [] ac
   where st   = top sk
         ac   = l_at st Dollar

lr_sem f semantics l_tt l_at sk inp@(h:t) = lr' f semantics l_tt l_at sk inp ac
  where  st   = top sk
         ac   = l_at st (T h)


lr' f (semantics,g) l_tt l_at sk inp (Reduce pr)
  = lr_sem f ((prName,pr):semantics,g)  l_tt l_at sk'' inp
  where sk'     = popN (sizeProd pr) sk         -- reducing the stack
        st      = top sk'                       -- acessing the state on its top
        st'     = l_tt st (lhs_prod pr)         -- moving to a new state
        sk''    = push st' sk'                  -- pushing the new state
        prName  = prodName pr (prods g)

lr' f semantics l_tt l_at sk (h:t) (Shift st) = lr_sem f semantics l_tt l_at sk' t
  where sk'     = push st sk

lr' f (semantics,g) l_tt l_at sk inp Accept = f True  (semantics,g)
lr' f (semantics,g) l_tt l_at sk inp Error  = f False (semantics,g)


aterm :: (Eq t, Eq nt, Show t, Show nt) =>
     Bool -> ([(String, [Symb t nt])], Cfg t nt) -> (Bool, NTree String)

aterm True  (semantic, g) = (True ,  toNTree g semantic)
aterm False (semantic, g) = (False,  NTree "invalid parse tree" [])

