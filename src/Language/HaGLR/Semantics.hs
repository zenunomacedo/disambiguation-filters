-----------------------------------------------------------------------------
-- |
-- Copyright   :  (c) Universidade do Minho, 2004
-- License     :  LGPL
--
-- Maintainer  :  João Fernandes
-- Stability   :  experimental
-- Portability :  portable
--
-- Construction of the Generalised (SLR1) Action Tables
--
-----------------------------------------------------------------------------


module Language.HaGLR.Semantics where


import Data.Char
import Data.List
import Data.Maybe (maybe, catMaybes, fromJust)
import Control.Monad.State (State, get, put, evalState)

import Language.HaGLR.Cfg
import Language.HaGLR.LR_0
import Data.Tree.NTree.TypeDefs
import Data.Tree.Class

import Debug.Trace

-- CWI's ATerm (or Rose Trees in a functional setting)
-- changed to rose trees (Saraiva Dec 2016)

-- data ATerm   = AAppl AFun [ATerm]
--                   deriving (Eq,Show)
-- type AFun  = String


type Forest a = [NTree a]

-- collects labels of only one layer
subLabels :: NTree a -> [a]
subLabels (NTree _ sb) = map getNode sb

-- | The number of elements of a Rose Tree.
sizeNTree (NTree _ []) = 1
sizeNTree (NTree _ l ) = 1 + sum (map sizeNTree l)

-- | The elements of a Rose Tree in pre-order.
flatten :: NTree a -> [a]
flatten t = flatten' t []
  where flatten' (NTree x ts) xs = x:(foldr flatten' xs ts)

-- | Lists of nodes at each level of the tree.
levels' t = map (map getNode) $
                takeWhile (not . null) $ iterate (concatMap getChildren) t

levels :: NTree a -> [[a]]
levels (NTree v []) = [[v]]
levels (NTree v sb) = [v] : (foldr1 (zipWith' (++)) vsb)
   where vsb =  map levels sb

zipWith' :: (a -> a -> a) -> [a] -> [a] -> [a]
zipWith' f l [] = l
zipWith' f [] r = r
zipWith' f (h1:t1) (h2:t2) = (f h1 h2) : zipWith' f t1 t2


-- | List of leaves at each level of a given rose tree.
leavesPerLevel :: NTree a -> [a]
leavesPerLevel rt = concat $ leavesPerLevel' rt

leavesPerLevel' :: NTree a -> [[a]]
leavesPerLevel' (NTree v []) = [[v]]
leavesPerLevel' (NTree v sb) = [] : (foldr1 (zipWith' (++)) vsb)
   where vsb =  map leavesPerLevel' sb


-- | The leaves of a Rose Tree in pre-order.
rTreeLeaves :: NTree a -> [a]
rTreeLeaves (NTree v [])  = [v]
rTreeLeaves (NTree _ rts) = concatMap rTreeLeaves rts


nextLeaf :: [NTree a] -> Maybe a
nextLeaf rts = nextLeafSat (const True) rts

-- next leaf satisfying a condition
nextLeafSat :: (a -> Bool) -> [NTree a] -> Maybe a
nextLeafSat p [] = Nothing
nextLeafSat p (x:xs) = case nextLeafSat' p x of
  Nothing -> nextLeafSat p xs
  any     -> any
  where nextLeafSat' :: (a -> Bool) -> NTree a -> Maybe a
        nextLeafSat' p rt = case filter p (rTreeLeaves rt) of
          []     -> Nothing
          leaves -> Just (head leaves)


-- return all nodes and their frontiers which matches the given condition p. The result is a list.
-- seems very inefficient
succLeaves :: (a -> Bool) -> NTree a -> [(NTree a, [a])]
succLeaves p rt@(NTree v []) = [] -- if p v then [(rt, [])] else []
succLeaves p rt@(NTree v chld) =
  if p v
    then [(rt, [])] ++ concatMap (succLeaves p) chld
    else findChildren chld
  where
    findChildren [] = []
    findChildren (c:cs) = case succLeaves p c of
      [] -> findChildren cs
      nodeAndLeafs ->
        let followingLeaves = concatMap rTreeLeaves cs
            followingRes = concatMap (succLeaves p) cs
        in  map (\(rt, leaves) -> (rt, leaves ++ followingLeaves)) nodeAndLeafs ++ followingRes



-- return the fist node that matches the given condition p and its frontiers
fstSuccLeaves :: (a -> Bool) -> NTree a -> Maybe (NTree a, [a])
fstSuccLeaves p rt@(NTree v []) = Nothing -- if p v then Just (rt, []) else Nothing
fstSuccLeaves p rt@(NTree v chld) =
  if p v then Just (rt, []) else findChildren chld
  where
    findChildren [] = Nothing
    findChildren (c:cs) = case fstSuccLeaves p c of
      Nothing -> findChildren cs
      Just (n, retLeaves) -> Just (n, retLeaves ++ concatMap rTreeLeaves cs)



-- | Map on rose Trees.
mapNTree :: (a -> b) -> NTree a -> NTree b
mapNTree f (NTree v sf) = NTree (f v) (map (mapNTree f) sf)


-- | Stop Top Down Strategy for transforming rose trees.
stopTD_NTree' :: Eq a => (NTree a -> NTree a) -> a -> NTree a -> NTree a
stopTD_NTree' fw i rt@(NTree v sf)
                       | i == v    = fw rt
                       | otherwise = NTree v (map (stopTD_NTree' fw i) sf)

-- | Stop Top Down Strategy for transforming rose trees.
stopTD_NTree :: Eq a =>
                (NTree a -> NTree a) -> (a -> Bool) -> NTree a -> NTree a
stopTD_NTree fw p rt@(NTree v sf)
                       | p v       = fw rt
                       | otherwise = NTree v (map (stopTD_NTree fw p) sf)

-- | Stop Top Down Strategy for transforming rose trees.
-- the predicate (NTree a -> Bool) is more general
stopTD_NTree2 :: Eq a =>
                (NTree a -> NTree a) -> (NTree a -> Bool) -> NTree a -> NTree a
stopTD_NTree2 fw p rt@(NTree v sf)
                       | p rt      = fw rt
                       | otherwise = NTree v (map (stopTD_NTree2 fw p) sf)


-- Monad version of Stop Top Down Strategy for transforming rose trees.
-- the predicate, of type (NTree a -> Bool) is more general.
stopTD_NTreeM :: (Monad m, Eq a) =>
                 (NTree a -> m (NTree a)) -> (NTree a -> Bool) ->
                 NTree a -> m (NTree a)
stopTD_NTreeM mfw p rt@(NTree v sf)
                       | p rt      = mfw rt
                       | otherwise = do
                        sf' <- mapM (stopTD_NTreeM mfw p) sf
                        return $ NTree v sf'

-- Stop Down Top Strategy for transforming rose trees.
-- the predicate, of type (NTree a -> Bool) is more general.
stopDT_NTree :: Eq a =>
                (NTree a -> NTree a) -> (NTree a -> Bool) -> NTree a -> NTree a
stopDT_NTree fw p rt@(NTree v sf) =
  let sf' = map (stopDT_NTree fw p) sf
      rt' = NTree v sf'
  in  if p rt' then fw rt' else  rt'

-- Monad version of stop Down Top Strategy for transforming rose trees.
-- the predicate, of type (NTree a -> Bool) is more general.
stopDT_NTreeM :: (Monad m, Eq a) =>
                 (NTree a -> m (NTree a)) -> (NTree a -> Bool) -> NTree a -> m (NTree a)
stopDT_NTreeM fw p rt@(NTree v sf) = do
  sf' <- mapM (stopDT_NTreeM fw p) sf
  let rt' = NTree v sf'
  if p rt' then fw rt' else return rt'


-- Used when only the side effect matters
stopDT_NTreeM' :: (Monad m, Eq a) =>
                  (NTree a -> m ()) -> (NTree a -> Bool) -> NTree a -> m ()
stopDT_NTreeM' fw p rt@(NTree v sf) = do
  mapM (stopDT_NTreeM' fw p) sf
  if p rt then fw rt else return ()



-- | Fold on rose trees.
foldNTree :: (a -> [b] -> b) -> NTree a -> b
foldNTree f = foldNTree'
  where foldNTree' (NTree v sf) = f v (map foldNTree' sf)

-- | Build a rose tree from a given value
unfoldNTree :: (b -> (a, [b])) -> b -> NTree a
unfoldNTree f b = let (a, bs) = f b
                  in  NTree a (unfoldRForest f bs)

-- | Build a forest from a list of values
unfoldRForest :: (b -> (a, [b])) -> [b] -> Forest a
unfoldRForest f l = map (unfoldNTree f) l



data Path =
    Path [Int]
  | Here
  deriving (Show, Eq)

-- return all paths of the nodes satisfying the condition
rTreePaths :: (NTree a -> Bool) -> NTree a -> Maybe [Path]
rTreePaths p tree@(NTree v rts)
  | p tree = Just [Here]
  | not (p tree) && null rts = Nothing
  | otherwise = Just . concat . catMaybes $ map subPaths (zip rts [0..])
  where
    subPaths (tree', pos) = case rTreePaths p tree' of
      Just paths ->
        Just $ map (\path -> case path of
          Here    -> Path [pos]
          Path pa -> Path (pos:pa)) paths
      Nothing -> Nothing

rTreePathLast :: (NTree a -> Bool) -> NTree a -> Maybe Path
rTreePathLast p tree = case rTreePaths p tree of
  Nothing -> Nothing
  Just xs -> Just (last xs)

-- walk the path in a rose tree and apply a transformation at the last node
applyTransToPath :: Path -> (NTree a -> NTree a) -> NTree a -> NTree a
applyTransToPath (Path [i]) f (NTree v rts) =
  let (pre, focus, after) = splitListAt i rts
  in  NTree v  $  pre ++ [f focus] ++ after
applyTransToPath (Path (i:xs)) f (NTree v rts) =
  let (pre, focus, after) = splitListAt i rts
  in  NTree v  $  pre ++ [applyTransToPath (Path xs) f focus] ++ after

splitListAt idx l = (take idx l, l !! idx, drop (idx + 1) l)




-- why do we need to use “reverse” functions many times?

-- toNTree  :: (Eq t, Eq nt, Show t, Show nt) =>
--      Cfg t nt -> [(String, [Symb t nt])] -> NTree String
-- toNTree g ((n,p):ps) =
--      reverseATerm $  NTree n . fst $ toNTreeStr' g ( (n, reverse (tail p)) :ps)
--   -- (tail p) produces the RHS.


-- toNTreeStr' :: (Show nt, Show t, Eq nt, Eq t) =>
--         Cfg t nt
--      -> [(String, [Symb t nt])]
--      -> ([NTree [Char]], [(String, [Symb t nt])])
-- toNTreeStr' g ((_,[]):ps) = ([],ps)
-- toNTreeStr' g ((n,h:t):ps)
--       | is_terminal h g = let (r',ps') =  toNTreeStr' g ((n,t):ps)
--                           in  ([NTree (showSymb h) []]++r',ps')
--       | otherwise       = let ((pName, pr),prs) = prodNt h ps
--                               rhs_p             = tail pr
--                               (r',prs')         = toNTreeStr' g ((pName,rhs_p):prs)
--                               (r'',prs'')       = toNTreeStr' g ((n,t):prs')
--                           in  ([NTree pName r']++r'',prs'')


-- prodNt :: (Show t, Eq t) => t -> [(String, [t])] -> ((String, [t]), [(String, [t])])
-- prodNt nt [] = ((show nt,[]),[])
-- prodNt nt ((n,h):t) | head h == nt = ((n,h),t)  -- should head h === n hold? (No.)
--                     | otherwise = let ((n',h'),t') = prodNt nt t
--                                   in  ((n',h'),(n,h):t')


toNTree :: (Eq t, Show t, Eq nt, Show nt) =>
           Cfg t nt -> [(String, [Symb t nt])] -> NTree String
toNTree g ((n,p):ps) =
  mapNTree (\(v,_) -> v) . toNTree' ps . markRTFalse' $
    NTree n (map symbToNTreeStr (tail p))

-- the Bool flag is needed to check whether the production has been expanded.
-- because of the production rules... we cannot simply check it according to
-- whether it has subtrees.
toNTree' :: (Eq t, Show t, Eq nt, Show nt) =>
            [(String, [Symb t nt])] -> NTree (String, Bool) -> NTree (String, Bool)
toNTree' [] acc = acc
toNTree' ((n,p):ps) acc =
  let nextNT = head p
      rMostPath = rTreePathLast (predicate nextNT) acc
      acc' = applyTransToPath (fromJust rMostPath) (expandNT (n, tail p)) acc
  in  toNTree' ps acc'
  where
    expandNT (n, p) (NTree (_, False) []) = markRTFalse' $ NTree n (map symbToNTreeStr p)

    predicate nt'@(NT _) (NTree (nt, False) []) = showSymb nt' == nt
    predicate _ _ = False


symbToNTreeStr :: (Show t, Show nt) => Symb t nt -> NTree String
symbToNTreeStr sym   = NTree (showSymb sym) []





toNTreeSymb :: (Eq t, Show t, Eq nt, Show nt) =>
           Cfg t nt -> [(String, [Symb t nt])] -> NTree (Symb Char String)
toNTreeSymb g ((n,p):ps) =
  mapNTree (\(v,_) -> v) . toNTreeSymb' ps . markRTFalse' $
    NTree (NT n) (map symbToNTree (tail p))


-- the Bool flag is needed to check whether the production has been expanded.
-- because of the production rules... we cannot simply check it according to
-- whether it has subtrees.
toNTreeSymb' :: (Eq t, Show t, Eq nt, Show nt) =>
            [(String, [Symb t nt])] -> NTree (Symb Char String, Bool) ->
            NTree (Symb Char String, Bool)
toNTreeSymb' [] acc = acc
toNTreeSymb' ((n,p):ps) acc =
  let nextNT = head p
      rMostPath = rTreePathLast (predicate nextNT) acc
      acc' = applyTransToPath (fromJust rMostPath) (expandNT (n, tail p)) acc
  in  toNTreeSymb' ps acc'
  where
    expandNT (n, p) (NTree (nt, False) []) = markRTFalse' $ NTree (NT n) (map symbToNTree p)

    predicate nt'@(NT _) (NTree (nt@(NT _), False) []) = showSymb nt' == showSymb nt
    predicate _ _ = False


symbToNTree :: (Eq t, Show t, Eq nt, Show nt) =>
               Symb t nt -> NTree (Symb Char String)
symbToNTree t@(T _)   = NTree (T . head . showSymb $ t) []
symbToNTree nt@(NT _) = NTree (NT . showSymb $ nt) []


markRTFalse :: NTree a -> NTree (a, Bool)
markRTFalse = mapNTree (\v -> (v,False))

-- mark subtrees to False flag while marking the root to True flag
markRTFalse' :: NTree a -> NTree (a, Bool)
markRTFalse' (NTree v rts) = NTree (v,True) (map (mapNTree (\v -> (v,False))) rts)


-- toNTreeSymb  :: (Eq t, Show t, Eq nt, Show nt) =>
--      Cfg t nt -> [(String, [Symb t nt])] -> NTree (Symb Char String)
-- toNTreeSymb g ((n,p):ps) =
--   reverseATerm $ NTree (NT n) . fst $ toNTreeSymb' g ( (n, reverse (tail p)) :ps)


-- toNTreeSymb' :: (Eq t, Show t, Eq nt, Show nt) =>
--         Cfg t nt
--      -> [(String, [Symb t nt])]
--      -> ([NTree (Symb Char String)], [(String, [Symb t nt])])
-- toNTreeSymb' g ((_,[]):ps) = ([],ps)
-- toNTreeSymb' g ((n,h:t):ps) -- n :: String ; h :: Symb t nt;  t :: [Symb t nt]  ; ps :: [(nt, [Symb t nt])]
--       | is_terminal h g = let (r',ps') =  toNTreeSymb' g ((n,t):ps)
--                           in  ( [NTree (T . head . showSymb $ h) []] ++ r', ps' )
--       | otherwise       = let ((pName, pr), prs) = prodNt h ps
--                               rhs_p              = tail pr
--                               (r',prs')          = toNTreeSymb' g ((pName,rhs_p):prs)
--                               (r'',prs'')        = toNTreeSymb' g ((n,t):prs')
--                           in  ([NTree (NT pName) r'] ++ r'', prs'')


reverseATerm :: NTree a -> NTree a
reverseATerm (NTree x []) = NTree x []
reverseATerm (NTree x  l) = NTree x (reverse (map reverseATerm l))


toNTreeTest :: (Eq t, Show t, Eq nt, Show nt) =>
           [(String, [Symb t nt])] -> NTree String
toNTreeTest (a:b) =  snd $ toNTreeTest' a b--trace ("\n\n" ++ (show (a:b)) ++ "\n\n") $ toNTree undefined (a:b)


toNTreeTest' :: (Eq t, Show t, Eq nt, Show nt) =>
             (String, [Symb t nt]) -> [(String, [Symb t nt])] -> ([(String, [Symb t nt])], NTree String)

toNTreeTest' (s, [lhs]) r = (r, mkTree s [])

toNTreeTest' (s, (lhs: [k@(T x)] )) r = (r, mkTree s [mkTree (showSymb k) []]) 

--this case implies that a token is being read - for example, "let" is interpreted as 'l', 'e', 't', in which each character is a terminal symbol
toNTreeTest' (s, (lhs: ((k@(T x)):[p_rest]) )) r@((s2, lhs2 : rhs2):xs) = (rest1, mkTree s [mkTree (showSymb k) [], child1]) 
 where ( rest1 , child1@(NTree rec1 gchild1) ) = toNTreeTest' (s2, lhs2:rhs2) xs 

toNTreeTest' (s, lhs : [p1]) r@((s2, lhs2 : rhs2):xs) = (rest1, mkTree s [NTree rec1 child1])  
 where ( rest1 , NTree rec1 child1 ) = toNTreeTest' (s2, lhs2:rhs2) xs 

toNTreeTest' (s, lhs : (p1:ps)) r@((s2, lhs2 : rhs2):xs) = (rest2, NTree rec2 (child2 ++ [child1]))
  where ( rest1 , child1@(NTree rec1 gchild1) ) = toNTreeTest' (s2, lhs2:rhs2) xs
        ( rest2 , NTree rec2 child2 ) = toNTreeTest' (s, lhs : ps) rest1

toNTreeTest' s r = error $ "Do you think God stays in Heaven because he too lives in fear of what he's created?"


--GLRActionsTable: ("ntFromT29",[NT "ntFromT291",NT "ntFromT292"])
--[]
