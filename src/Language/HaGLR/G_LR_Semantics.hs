-----------------------------------------------------------------------------
-- |
-- Copyright   :  (c) Universidade do Minho, 2004
-- License     :  LGPL
--
-- Maintainer  :  João Fernandes
-- Stability   :  experimental
-- Portability :  portable
--
-- Generalised LR parsing + construction of a XML and, or ATerm AST
--
-----------------------------------------------------------------------------

module Language.HaGLR.G_LR_Semantics
    ( glr_aterm
    , glr_sem
    , glr_atermSymb
    , glr_sem'
    , glr_aterm'
    ) where

-----------------------------------------------------------------------------

import Language.HaLex.Dfa
import Language.HaGLR.Stack
import Language.HaGLR.Cfg
import Language.HaGLR.LR_0
import Language.HaGLR.SLR
import Language.HaGLR.G_SLR
import Language.HaGLR.Semantics

import Data.Tree.NTree.TypeDefs

import Debug.Trace
-----------------------------------------------------------------------------

glr_aterm :: (Ord t,Ord nt,Show t,Show nt)
          => Cfg t nt
    -> [t]
    -> [NTree String]
glr_aterm = semantics_glraccept aterm


glr_atermSymb :: (Ord t, Show t, Ord nt, Show nt)
              => Cfg t nt
        -> [t]
        -> [NTree String]
glr_atermSymb a aa = trace ("input: " ++ show aa) $ semantics_glraccept atermSymb a aa


-- | Generalised LR grammar interpreter that builds an AST

semantics_glraccept :: (Eq t, Ord t, Ord nt, Show t, Show nt) =>
     (Bool  ->  ([([Char], [Symb t nt])], Cfg t nt)  ->  [(Bool, sem)])
  ->  Cfg t nt  ->  [t]  ->  [NTree String]
semantics_glraccept f g inp =  glr_sem f semantics lookupTT_g lookupAT_g [s] inp
  where eg@(Cfg t nt r p)   = expand_cfg g      -- The expanded grammar
        dfa@(Dfa v q s z d) = ecfg2LR_0Dfa eg   -- The LR_0 DFA

        at                  = e_gslr_at  eg     -- The Action Table

        lookupAT_g          = glookupAT t at    -- The lookup fun. for the AT
        lookupTT_g          = d                 -- The lookup fun. for the TT
        semantics           = ([],g)



glr_sem :: (Eq nt1, Eq t2, Show t2, Show nt1) =>
        (Bool -> ([(String, [Symb t2 nt1])], Cfg t2 nt1) -> [(Bool, sem)])
     -> ([(String, [Symb t2 nt1])], Cfg t2 nt1)
     -> (a -> Symb t2 nt1 -> a)
     -> (a -> Symb t nt -> [Action a [Symb t2 nt1]])
     -> [a]
     -> [t]
     -> [NTree String]
glr_sem f semantics l_tt l_at sk [] =
        [ res | ac  <- acs
        , res <-  glr' f semantics l_tt l_at sk [] ac ]
   where st   = top sk
         acs  = l_at st Dollar

glr_sem f semantics l_tt l_at sk inp@(h:t) =
        [ res | ac <- acs
        , res <- glr' f semantics l_tt l_at sk inp ac ]
  where  st   = top sk
         acs  = l_at st (T h)    -- It returns a list of actions


glr' :: (Eq nt1, Eq t2, Show t2, Show nt1) =>
        (Bool -> ([(String, [Symb t2 nt1])], Cfg t2 nt1) -> [(Bool, sem)])
     -> ([(String, [Symb t2 nt1])], Cfg t2 nt1)
     -> (a -> Symb t2 nt1 -> a)
     -> (a -> Symb t nt -> [Action a [Symb t2 nt1]])
     -> [a]
     -> [t]
     -> Action a [Symb t2 nt1]
     -> [NTree String]
glr' f (semantics,g) l_tt l_at sk inp (Reduce pr)
  = glr_sem f ((prName,pr):semantics,g)  l_tt l_at sk'' inp
  where sk'     = popN (sizeProd pr) sk         -- reducing the stack
        st      = top sk'                       -- acessing the state on its top
        st'     = l_tt st (lhs_prod pr)         -- moving to a new state
        sk''    = push st' sk'                  -- pushing the new state
        prName  = prodName pr (prods g)

glr' f semantics l_tt l_at sk (h:t) (Shift st) = glr_sem f semantics l_tt l_at sk' t
  where sk'     = push st sk

glr' f (semantics,g) l_tt l_at sk inp Accept = return $ toNTreeTest semantics
glr' f (semantics,g) l_tt l_at sk inp Error  = []

-- aterm :: (Show nt, Show t, Eq nt, Eq t) =>
--    Bool -> ([(String, [Symb t nt])], Cfg t nt) -> [(Bool, NTree String)]
aterm True  (semantic, g) = [(True,  toNTree g semantic)]
aterm False (semantic, g) = [(False, NTree "invalid parse tree" [])]


atermSymb True  (semantic, g) = [(True,  toNTreeSymb g semantic)]
-- do not know how to give a default value of proper type
atermSymb False (semantic, g) = [(False, NTree (NT "invalid parse tree") [])]










glr_aterm' :: (Ord t,Ord nt,Show t,Show nt)
           => Cfg t nt
     -> [t]
     -> [(Bool, [(String , [Symb t nt])] )]
glr_aterm' = semantics_glraccept' id 


semantics_glraccept' f g inp =  glr_sem' f semantics lookupTT_g lookupAT_g [s] inp
  where eg@(Cfg t nt r p)   = expand_cfg g      -- The expanded grammar
        dfa@(Dfa v q s z d) = ecfg2LR_0Dfa eg   -- The LR_0 DFA

        at                  = e_gslr_at  eg     -- The Action Table

        lookupAT_g          = glookupAT t at    -- The lookup fun. for the AT
        lookupTT_g          = d                 -- The lookup fun. for the TT
        semantics           = ([],g)




glr_sem' f semantics l_tt l_at sk [] =
        [ res | ac  <- acs
              , res <- glr'' f semantics l_tt l_at sk [] ac
  ]
   where st   = top sk
         acs  = l_at st Dollar

glr_sem' f semantics l_tt l_at sk inp@(h:t) =
        [ res | ac <- acs
              , res <- glr'' f semantics l_tt l_at sk inp ac
  ]
  where  st   = top sk
         acs  = l_at st (T h)    -- It returns a list of actions



glr'' f (semantics,g) l_tt l_at sk inp (Reduce pr)
  = glr_sem' f ((prName,pr):semantics,g)  l_tt l_at sk'' inp
  where sk'     = popN (sizeProd pr) sk         -- reducing the stack
        st      = top sk'                       -- acessing the state on its top
        st'     = l_tt st (lhs_prod pr)         -- moving to a new state
        sk''    = push st' sk'                  -- pushing the new state
        prName  = prodName pr (prods g)

glr'' f semantics l_tt l_at sk (h:t) (Shift st)
  = glr_sem' f semantics l_tt l_at sk' t
  where sk'     = push st sk

glr'' f (semantics,g) l_tt l_at sk inp Accept = [(True,semantics)]
glr'' f (semantics,g) l_tt l_at sk inp Error  = [(False,semantics)]



