-----------------------------------------------------------------------------
-- |
-- Copyright   :  (c) Universidade do Minho, 2004
-- License     :  LGPL
--
-- Maintainer  :  João Saraiva - jas@di.uminho.pt
-- Stability   :  experimental
-- Portability :  portable
--
-- Construction of the Generalised (SLR1) Action Tables
--
-----------------------------------------------------------------------------

module Language.HaGLR.G_SLR
  ( gslr_at             -- computes slr action table
  , e_gslr_at
  , pair_ecfg2LR_0Dfa_e_gslr_at
  -- , pp_at               -- pretty prints the slr action table
  -- , gslrPPTable         -- pretty prints the gslr action table
  -- , gslrPPTable2File    -- pretty prints in a file the slr action table
  , glookupAT           -- look up function for the action table
  , glookupAT'
  ) where

-----------------------------------------------------------------------------

import Data.List
import Language.HaLex.FaOperations
-- import Language.HaGLR.Tables
import Language.HaGLR.Cfg
import Language.HaGLR.LR_0
import qualified Language.HaGLR.SLR as SLR
import Debug.Trace


-- import PrettyPrint.Tables.Combinators

-----------------------------------------------------------------------------

-- The action table is defined as a list of rows, where each row consists
-- of the DFA state (first column) and the remain columns define the
-- action to be performed for each symbol of the vocabulary. While in a
-- deterministic parser each table entry has a unique action to be
-- performed (no conflicts are allowed), in this case we consider a list
-- of actions. Thus, the action table has the following type now:


type AT st pr = PTable st [SLR.Action st pr]

-- That is equivalent to
-- type AT st pr = [ (st , [[SLR.Action st pr]]) ]
-- row :: (st , [[SLR.Action st pr]])

-- Now, we define the function that constructs the SLR action table:

-- | Computes the Generalised SLR(1) tables for a given grammar.

gslr_at :: (Ord t, Ord nt)
        => Cfg t nt                           -- ^ Grammar
        -> AT [Item (Symb t nt)] [Symb t nt]    -- ^ Action table
gslr_at g = e_gslr_at $ expand_cfg g

-- | Computes the Generalised SLR(1) tables for a given expanded grammar.

e_gslr_at :: (Ord t, Ord nt)
          => Cfg t nt                  -- ^ Grammar
          -> AT [Item (Symb t nt)] [Symb t nt]  -- ^ Action table
e_gslr_at eg@(Cfg t nt s p) = trace ("e_gslr_at\n" ++ show (length action_table)) $ action_table
  where dfa_tt = trace "dfa_tt" $ elr_0_tt eg
        res    = trace "res" $ map (SLR.st_follows_shifts eg dfa_tt) (stsDfa dfa_tt)

        action_table = map (one_row_at t) res

pair_ecfg2LR_0Dfa_e_gslr_at g@(Cfg t nt s p) = (ecfg2LR_0Dfa_val, action_table)
 where aux = cfg2LR_0Ndfa g
       dfa_tt           = ndfa2ct aux
       ecfg2LR_0Dfa_val = ndfa2dfa' aux dfa_tt
       res              = map (SLR.st_follows_shifts g dfa_tt) (stsDfa dfa_tt)
       action_table     = map (one_row_at t) res


one_row_at ts (st,follows,shifts) = (st, [ g_action(t,st,follows,shifts)
                                         | t <- ts
                                         ]
                                    )

g_action (t,st,follows,shifts)
    | isFollow                                   = map (\ x -> SLR.Reduce x) reduceProd
    | isMove                                     = [SLR.Shift newState]
                                                       ++ map (\ x -> SLR.Reduce x) reduceProd
    | SLR.is_accepting_action st && t == Dollar  = [SLR.Accept]
    | otherwise                                  = [SLR.Error]
  where (isFollow,reduceProd) = getFollow t follows
        (isMove,newState)     = SLR.getState t shifts

getFollow t []         = (False,[])
getFollow t ((it,f):l) | t `elem` f = (True,[pr_of_it it] ++ (snd $ getFollow t l))
                       | otherwise  = getFollow t l


-- Tabels are easier to understand in a "table-like" reresentation. Thus,
-- we define now the pretty printing of such a table. We use the (LRC) table
-- pretty printers.


-- pp_at :: (Show a, Show b) => [(a,[b])] -> R
-- pp_at at = matrix2table'' at


-- gslrPPTable g =  asciiT' $ matrix2table''' (terminals g') at
--   where g' = expand_cfg g
--         at = e_gslr_at g'

-- gslrPPTable2File g fn =
--   do let pp = gslrPPTable g
-- --     let pp = matrix2tableList' (terminals g')  at
--      writeFile fn pp


-- Now, we define a lookup function on the action table, that given the
-- vocabulary, the action table, the state (row) and the grammar terminal
-- symbol (column), it returns the action to be performed. The vocabulary
-- is needed to be able to compute the column associated to the terminal
-- symbol (it corresponds to its position in the vocabulary list).

--lookupAT :: (Eq sy , Eq st)  => [Symb t nt] -> AT st pr -> st -> Symb t nt -> [Actions pr]
--glookupAT :: (Eq a1, Eq a) =>
--             [a] -> [(a1, [a2])] -> a1 -> a -> a2
glookupAT v at st sy = acts !! i
  where  [(_,acts)] = filter (\(st',acts) -> st == st') at
         (Just i) = elemIndex sy v

glookupAT' v at st sy = (at!!st) !! i
  where (Just i) = elemIndex sy v

