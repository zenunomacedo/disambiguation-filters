-----------------------------------------------------------------------------
-- |
-- Copyright   :  (c) Universidade do Minho, 2004, 2005, 2016
-- License     :  LGPL
--
-- Maintainer  :  Jo�o Saraiva - saraiva@di.uminho.pt
-- Stability   :  experimental
-- Portability :  portable
--
-- Representing Grammars as Finite Automata, i.e., as LR(0) Automata
--
-----------------------------------------------------------------------------

module Language.HaGLR.LR_0
  ( Item
  , is_reducing_it
  , reducing_its
  , after_dot
  , pr_of_it
  , showItems
  , expand_cfg
  , elr_0_tt
  , cfg2LR_0Ndfa
  , cfg2LR_0Dfa
  , ecfg2LR_0Dfa
  , lr_0_tt
  , ndfa2dfa'
  , lookupCT
  , is_accepting_item
  , saveLR_0Ndfa
  , saveLR_0Dfa
  , saveLR_0NdfaIO
  , saveLR_0DfaIO
  -- , pp_tt
  -- , save_trans_table
  -- , cfg2lr_0NdfaAscii
  -- , cfg2lr_0NdfaHtml
  -- , items2table
  -- , item2table
  -- , showItemHTML , showItemAscii , its , showrhs , showLL
  ) where

------------------------------------------------------------------------------

import Data.List
import Language.HaLex.Ndfa
import Language.HaLex.Dfa
import Language.HaLex.Util as Util
import Language.HaLex.Util (limit)
import qualified Language.HaLex.FaOperations as FaOperations
import Language.HaLex.FaAsDiGraph
import qualified Data.Map.Strict as Map
-- import PrettyPrint.Tables.Combinators

-- import Language.HaGLR.Tables
import Language.HaGLR.Cfg

------------------------------------------------------------------------------

-- | To represent grammars as LR(0) automata, the produtions are defined as
--   'LR(0) items' which are used to define the automata states.
--   A LR(0) item is a production with a 'dot' in its right-hand side: it indicates
--   how much of a production has been seen at a given point in the parsing process.


-- | To model LR(0) items we introduce the 'Item' data type

type Item sy = ([sy],Int)


-- | Usually a LR(0) item is denoted as defined in the next show function.

showItem (p,i) = (showSymb h) ++ " -\\> " ++ (showLL $ take (i-1) t) ++ " . " ++ (showLL $ drop (i-1) t)
  where h = head p
        t = tail p

showLL []    = ""
showLL (h:t) = (showSymb h) ++ " " ++ (showLL t)

showItems its = map showItem its

-- | When the 'dot' is after the last symbol of the production, the item is called a reducing item.
--   (the production's RHS can be reduced to its left-had side)

is_reducing_it :: Item sy -> Bool
is_reducing_it (p,i) = i == length p

reducing_its :: [Item sy] -> [Item sy]
reducing_its = filter is_reducing_it

-- | Computes the production symbols after the dot.

after_dot :: Item sy -> [sy]
after_dot (p,i) = drop i p

-- | Prodution of a given 'Item'

pr_of_it :: Item sy -> [sy]
pr_of_it = fst








-- | Expands a given CFG with the new root symbol S' and production S' -> S $
--
expand_cfg :: Cfg t nt  -> Cfg t nt
expand_cfg (Cfg t nt s p) = Cfg t' nt' s' p'
  where t'   = t ++ [Dollar]
        nt'  = Root : nt
        s'   = Root
        p'   = ("NewRoot",(Root |-> [s,Dollar])) : p



-- type CT st = [(Item st,[[Item st]])]

type CT st = PTable (Item st) [Item st]



stsNdfa :: CT sy -> [Item sy]
stsNdfa = map fst

stsRHS   = map snd

allstsCT :: Eq sy => CT sy -> [Item sy]
allstsCT = nub . concat . concat . stsRHS

showCT ct = map showCTLine ct
  where showCTLine (l,r) = showItem l ++ show (concat $ map showItems r) ++ "\n"

aux (NT x) = x
aux Root = Root

walk_epsilon :: (Eq t, Eq nt) => Cfg t nt -> Item (Symb t nt) -> [Item (Symb t nt)]
walk_epsilon g (p,i) | i < length p = new_items
                     | otherwise    = []
  where nt        = p !! i
        ps        = prods_nt g nt
        new_items = map (\ sy -> (sy,1)) ps

walk_symb :: Eq sy => Item sy -> sy -> [Item sy]
walk_symb (p,i) sy | (i < length p) && (p !! i) == sy = [(p,i+1)]
                   | otherwise                        = []

oneRow :: (Eq t, Eq nt) => Cfg t nt -> Item (Symb t nt) -> [Symb t nt] -> [[Item (Symb t nt)]]
oneRow g st alfabet = (walk_epsilon g st)
                          : (map (\ v -> walk_symb st v) alfabet)

consRows :: (Eq t, Eq nt) => Cfg t nt -> [Item (Symb t nt)] -> [Symb t nt] -> CT (Symb t nt)
consRows g []     alfabet = []
consRows g (q:qs) alfabet = (q , oneRow g q alfabet) : (consRows g qs alfabet)


cfg2CtStep :: (Eq t, Eq nt) => Cfg t nt -> [Symb t nt] -> CT (Symb t nt) -> CT (Symb t nt)
cfg2CtStep g alfabet ct = nub (ct `union` consRows g newSts alfabet)
  where newSts =  (allstsCT ct) Util.<-> (stsNdfa ct)


ecfg2ct :: (Eq t, Eq nt) => Cfg t nt -> CT (Symb t nt)
ecfg2ct g@(Cfg t nt s p) = limit (cfg2CtStep g v)  ttFstRow
  where v        = t ++ nt
        ttFstRow =  consRows g [(Root |-> [s,Dollar],1)] v

cfg2ct :: (Eq t, Eq nt) => Cfg t nt ->  CT (Symb t nt)
cfg2ct = ecfg2ct . expand_cfg


cfg2LR_0Ndfa :: (Ord t, Ord nt)
             => Cfg t nt
             -> Ndfa (Item (Symb t nt)) (Symb t nt)
cfg2LR_0Ndfa g@(Cfg t nt s p) = Ndfa v' q' s' z' d'
  where -- g'@(Cfg t nt s p) = expand_cfg g
        tt = ecfg2ct g
        v' = t ++ nt
        q' = stsNdfa tt
        s' = [(Root |-> [s,Dollar],1)]
        z' = []
        d' st sy = lookupCT_fast st sy (Map.fromList tt) (Map.fromList $ zip v' [0..])
        --d' st sy = lookupCT st sy (tt) v'


lookupCT_fast st Nothing   mapL v = case (Map.lookup st mapL) of 
                                      (Just q) -> head q
                                      Nothing -> []
lookupCT_fast st (Just sy) mapL v = case (Map.lookup st mapL) of 
                                      (Just q) -> tail q !! col
                                      Nothing -> [] 
 where (Just col) = Map.lookup sy v --elemIndex sy v


-- type CT st = [(Item st,[[Item st]])]
-- type Item sy = ([sy],Int)


{-
lookupCT :: (Eq a,Eq (Symb a))
          => Item (Symb a)
          -> Maybe (Symb a)
          -> CT (Symb a)
          -> [Symb a]
          -> [Item (Symb a)]
-}
lookupCT st Nothing   (q:qs) v  | (fst q == st) = (head $ snd q)  -- epsilon is first column
                                | otherwise     = lookupCT st Nothing qs v
lookupCT st (Just sy) (q:qs) v  | (fst q == st) = (tail $ snd q) !! col
                                | otherwise     = lookupCT st (Just sy) qs v
   where (Just col) = elemIndex sy v
lookupCT _ _ [] _ = []



type StDfa st = [st]
ndfa2dfa' :: (Ord st,Ord sy) => Ndfa st sy -> FaOperations.CT st -> Dfa [st] sy
ndfa2dfa' ndfa@(Ndfa v q s z delta) dfa_tt = (Dfa v' q' s' z' delta')
  where  tt = dfa_tt
         v' = v
         q' = FaOperations.stsDfa tt
         s' = fst (head tt)
         z' = finalStatesDfa q' z
         delta' st sy = lookupCTf st sy (Map.fromList tt) (Map.fromList $ zip v [0..])

finalStatesDfa :: Eq st => [StDfa st] -> [st] -> [StDfa st]
finalStatesDfa []     z = []
finalStatesDfa (q:qs) z | (q `intersect` z /= []) = q : finalStatesDfa qs z
                        | otherwise               = finalStatesDfa qs z

-- lookupCT :: (Eq st, Eq sy) => [st] -> sy -> CT st -> [sy] -> StDfa st
lookupCTf st sy l v = case Map.lookup st l of 
            Just k -> k !! col
            Nothing -> []
 where (Just col) = Map.lookup sy v--elemIndex sy v
{-lookupCTf st sy (q:qs) v  | (fst q == st) = (snd q) !! col
                          | otherwise     = lookupCTf st sy qs v
   -}








cfg2LR_0Dfa :: (Ord t, Ord nt) => Cfg t nt -> Dfa [Item (Symb t nt)] (Symb t nt)
cfg2LR_0Dfa = ecfg2LR_0Dfa . expand_cfg

ecfg2LR_0Dfa :: (Ord t,Ord nt) => Cfg t nt -> Dfa [Item (Symb t nt)] (Symb t nt)
ecfg2LR_0Dfa = FaOperations.ndfa2dfa . cfg2LR_0Ndfa


lr_0_tt :: (Ord t,Ord nt) => Cfg t nt -> FaOperations.CT (Item (Symb t nt))
lr_0_tt = elr_0_tt . expand_cfg

elr_0_tt :: (Ord t, Ord nt) => Cfg t nt -> FaOperations.CT (Item (Symb t nt))
elr_0_tt g = FaOperations.ndfa2ct $ cfg2LR_0Ndfa g


-- | An Item is an accepting item iff the dot is just before the 'Dollar'
--is_accepting_item :: Eq sy => Item (Symb sy) -> Bool
is_accepting_item it = if (not (is_reducing_it it)) &&
                          ((head $ after_dot it) == Dollar) then True
                       else False

saveLR_0Ndfa :: (Eq t, Ord t, Show t,  Eq nt, Ord nt, Show nt)
             => Cfg t nt -> [Char]
saveLR_0Ndfa g = tographviz' ndfa "lr_0_ndfa" "record" "LR" showItem showSymb True False
  where ndfa   = cfg2LR_0Ndfa $ expand_cfg g


saveLR_0NdfaIO
  :: (Ord t, Ord nt, Show t, Show nt) => Cfg t nt -> IO ()
saveLR_0NdfaIO g =
   tographvizIO' ndfa "lr_0_ndfa" "record" "LR" showItem showSymb True False
  where ndfa     = cfg2LR_0Ndfa $ expand_cfg g


saveLR_0Dfa g = tographviz' ndfa' "lr_0_dfa" "record" "LR"
                           (concatDOT . showItems) showSymb True False
  where ndfa  = cfg2LR_0Ndfa $ expand_cfg g
        dfa   = FaOperations.ndfa2dfa ndfa
        ndfa' = FaOperations.dfa2ndfa dfa

saveLR_0DfaIO g = tographvizIO' ndfa' "lr_0_dfa" "record" "LR"
                               (concatDOT . showItems) showSymb True False
  where ndfa  = cfg2LR_0Ndfa $ expand_cfg g
        dfa   = FaOperations.ndfa2dfa ndfa
        ndfa' = FaOperations.dfa2ndfa dfa

concatDOT []     = ""
concatDOT (h:[]) = h
concatDOT (h:t)  = h ++ ['\92','l'] ++ (concatDOT t)
-- concatDOT (h:t)  = h ++ "\\l" ++ (concatDOT t)


-- pp_tt :: (Show a,Show b) => [(s,[b])] -> R


-- pp_tt ct = matrix2table'' ct

-- save_trans_table g fn = do let tt = pp_tt $ lr_0_tt g
--                            writeFile fn (asciiT' tt)




-- cfg2lr_0NdfaAscii g fn = ppPTable show show ct
--    where ct = FaOperations.ndfa2ct $ cfg2LR_0Ndfa g


-- cfg2lr_0NdfaHtml g fn = writeFile fn (ppPTableHtml showItemsHTML showItemsHTML ct)
--   where ct = FaOperations.ndfa2ct $ cfg2LR_0Ndfa g




-----------------------------------------------------------------------------
-- Working part
--


its :: [Item (Symb Char Char)]
its = [((NT 'A'|->[NT 'B',NT 'C',NT 'D']),2),(NT 'A'|-> [T 'a',NT 'A'],1)]


showItemHTML :: (Show nt , Show t) => Item (Symb t nt) -> String
showItemHTML ((h:t),i) = (showSymb h) ++ " -> " ++
                         (showLL $ take (i-1) t) ++ " . " ++ (showLL $ drop (i-1) t)

showItemsHTML its = concat $ map showItemHTML its



-- items2table :: (Show nt , Show t) => [Item (Symb t nt)] -> PPTable
-- items2table its = vlist' (map item2table its)

-- item2table :: (Show nt , Show t) => Item (Symb t nt) -> PPTable
-- item2table p =  text (showItemAscii p)

showItemAscii  :: (Show nt , Show t) => Item (Symb t nt) -> String
showItemAscii ((h:t),i) = (showSymb h) ++ " -> " ++
                          (showLL $ take (i-1) t) ++ " . " ++ (showLL $ drop (i-1) t)



-- showLR_0Table :: (Ord t , Ord nt , Show t , Show nt) => Cfg t nt -> PPTable
-- showLR_0Table g =   asciiT'' $ showrhs rhs              -- llist items2table showrhs tt
--  where dfa = ecfg2LR_0Dfa g
--        lhs  = fst $ head $ tail $ transitionTableDfa' dfa
--        rhs  = snd $ head $ tail $ transitionTableDfa' dfa

-- showrhs :: (Show t , Show nt) => [[Item (Symb t nt)]] -> PPTable
-- showrhs = hlist' . map items2table
-- showrhs rhs = hlist' (map items2table rhs)

{-
saveLR_0Table g fn = writeFile fn (asciiT'' ppct)
  where ppct = showLR_0Table g
-}

-- lr_0Ascii cfg = r
--   where dfa@(Dfa v _ _ _ _)  = ecfg2LR_0Dfa cfg
--         tt   = transitionTableDfa' dfa
--         hd = (color "red" "Q\\V") <|> hlist v
--         tl = llist items2table showrhs (tail tt)
--         table = hd PrettyPrint.Tables.Combinators.<-> tl

--         s = renderAscii $ items2table $ fst $ head $ tail tt
--         r = map asciiT''  (map items2table  $ snd $ head $ tail tt)

-- cfg2lr_0NdfaLatex

-- cfg2lr_0NdfaGraph


