
-----------------------------------------------------------------------------
-- |
-- Copyright   :  (c) Universidade do Minho, 2004
-- License     :  LGPL
--
-- Maintainer  :  Jo�o Saraiva - jas@di.uminho.pt
-- Stability   :  experimental
-- Portability :  portable
--
-- A trivial implementation of a stack
--
-----------------------------------------------------------------------------

module Language.HaGLR.Stack
  ( Stack
  , push
  , top
  , pop
  , popN
  ) where

-----------------------------------------------------------------------------

type Stack st = [st]

push = (:)
top  = head
pop  = tail
popN = drop
