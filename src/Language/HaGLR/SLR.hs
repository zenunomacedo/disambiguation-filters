-----------------------------------------------------------------------------
-- |
-- Copyright   :  (c) Universidade do Minho, 2004
-- License     :  LGPL
--
-- Maintainer  :  Jo�o Saraiva - jas@di.uminho.pt
-- Stability   :  experimental
-- Portability :  portable
--
-- Construction of the SLR(1) Action Tables
--
-----------------------------------------------------------------------------

module Language.HaGLR.SLR
  ( Action(..)          -- the type of the actions
  , slr_at              -- computes slr action table
  , e_slr_at
  , st_follows_shifts   -- computes the set of follows and shift symbols of a state
  , is_accepting_action
  , getState
  -- , pp_at               -- pretty prints the slr action table
  -- , slrPPTable          -- pretty prints the slr action table
  -- , slrPPTable2File     -- pretty prints in a file the slr action table
  -- , slrTables2ascii
  -- , slrTables2latex
  -- , slrTables2latex2file
  , lookupAT            -- look up function for the action table
  , lookupAT'
  ) where

------------------------------------------------------------------------------

import Data.List
import Language.HaLex.FaOperations as FaOperations
import Language.HaGLR.Cfg
-- import Language.HaGLR.Tables
import Language.HaGLR.LR_0

-- import PrettyPrint.Tables.Combinators

------------------------------------------------------------------------------

-- There are four types of actions in a shift-reduce parser:
--
--   Shift
--   Accept
--   Reduce
--   Error


-- To model these actions in Haskell we introduce a new data type, named
-- Action, with four constructor function, one per action.


data Action st pr = Shift st
                  | Accept
                  | Reduce pr
                  | Error
                  deriving (Eq,Show, Ord)


-- The action table is defined as a list of rows, where each row consists
-- of the DFA state (first column) and the remain columns define the
-- action to be performed for each symbol of the vocabulary.


-- type AT st pr = [ (st , [Actions st pr]) ]

type AT st pr = PTable st (Action st pr)


-- To construct the SLR(1) Action Tables automaton we compute the
-- following information:
--
--    - The follow sets of the left-hand-side symbol of a production
--      that defines a "reducing" item. This induces a reduce action in
--      the SLR action table.
--
--    - The set of symbols defining moves to other states. This induces
--      a shift action in the table.
--
--    - This induce an accept action in the table.
--
--    - All other cases induce a error action in the tabe.
--
-- Next, we define the function that computes the action table

-- | Computes the SLR(1) tables for a given grammar.


slr_at :: (Ord t, Ord nt)
       => Cfg t nt                             -- ^ Grammar
       -> AT [Item (Symb t nt)] [Symb t nt]    -- ^ Action table

slr_at g = e_slr_at g'
  where g'@(Cfg t nt s p) = expand_cfg g


-- | Computes the SLR(1) tables for a given expanded grammar.

e_slr_at :: (Ord t, Ord nt)
         => Cfg t nt                           -- ^ Grammar
         -> AT [Item (Symb t nt)] [Symb t nt]  -- ^ Action table

e_slr_at eg@(Cfg t nt s p) = action_table
  where dfa_tt = elr_0_tt eg
        res = map (st_follows_shifts eg dfa_tt) (FaOperations.stsDfa dfa_tt)

        action_table = map (one_row_at t) res

one_row_at ts (st,follows,shifts) = (st, [ action(t,st,follows,shifts)
                                         | t <- ts
                                         ]
                                    )


-- | For a given state, it accumulates the information needed to compure the SLR(1) action tables:
--         - the follow sets of each reducing item
--         - the destination state for each symbol

st_follows_shifts :: (Eq t, Eq nt)
                  => Cfg t nt                                        -- ^ Grammar
                  -> FaOperations.CT (Item (Symb t nt))                     -- ^ Goto Table induced by the grammar
                  -> [Item (Symb t nt)]                                     -- ^ State
                  -> ([Item (Symb t nt)],[(Item (Symb t nt),[Symb t nt])],[(Symb t nt,[Item (Symb t nt)])]) -- ^ Tripe (state,follows,moves)
st_follows_shifts g@(Cfg t nt s p) gt st = (st,zip red_its_of_st follows_red_its,shift_symb)
 where  red_its_of_st     = reducing_its st
        lhs_red_its_of_st = map (\(p,i) ->  lhs_prod p) red_its_of_st
        follows_red_its   = map ((follow g)) lhs_red_its_of_st

        shift_symb        = concat $ map f (map after_dot st)

        f []    = []
        f (x:_) | x `elem` (terminals g) = [(x,FaOperations.lookupCT st x gt (t ++ nt))]
                | otherwise              = []
        aux (NT x) = x



-- | Computes the action for each state based on the accumulated data.

action (t,st,follows,shifts) | is_accepting_action st && t == Dollar = Accept
                             | isMove                                = Shift newState
                             | isFollow                              = Reduce (head reduceProd)
                             | otherwise                             = Error
  where (isFollow,reduceProd) = getFollow t follows
        (isMove,newState)     = getState t shifts

getFollow t []         = (False,[])
getFollow t ((it,f):l) | t `elem` f = (True,[pr_of_it it])
                       | otherwise  = getFollow t l


getState t []          = (False,[])
getState t ((sy,st):l) | t == sy   = (True,st)
                       | otherwise = getState t l


is_accepting_action  :: (Eq t, Eq nt) => [Item (Symb t nt)] -> Bool
is_accepting_action st = or $ map is_accepting_item st


-- | Printing

-- Tabels are easier to understand in a "table-like" representation. Thus,
-- we define now the pretty printing of such a table. We use the (LRC) table
-- pretty printers.


-- pp_at :: (Show a, Show b) => [(a,[b])] -> R


-- pp_at at = matrix2table'' at


-- slrPPTable g = asciiT' $ matrix2table''' (terminals g') at
--   where g' = expand_cfg g
--         at = e_slr_at g'


{-
slrPPTable g = ppPTable (concat . showItems) (show) at
  where g' = expand_cfg g
        at = e_slr_at g'
-}

-- slrPPTable2File g fn = do let pp = slrPPTable g
--                           writeFile fn pp



-- slr_pp g = ppPTable' items2table (text . pp_action) at
--  where at = slr_at g


-- pp_action :: (Show t , Show nt)
--           => Action  [Item (Symb t nt)] [Symb t nt]
--           -> [Char]

-- pp_action (Shift st)    = "Shift " ++ (items2ascii st)
-- pp_action Accept        = "Accept"
-- pp_action (Reduce prod) = "Reduce " ++ (showProd' prod)
-- pp_action Error         = "Error"


-- Now, we define a lookup function on the action table, that given the
-- vocabulary, the action table, the state (row) and the grammar terminal
-- symbol (column), it returns the action to be performed. The vocabulary
-- is needed to be able to compute the column associated to the terminal
-- symbol (it corresponds to its position in the vocabulary list).

lookupAT :: (Eq t, Eq nt, Eq st)  => [Symb t nt] -> AT st pr -> st -> Symb t nt -> Action st pr
lookupAT v at st sy = acts !! i
  where  [(_,acts)] = filter (\(st',acts) -> st == st') at
         (Just i) = elemIndex sy v


lookupAT' v at st sy = (at!!st) !! i
  where (Just i) = elemIndex sy v





-- items2ascii its = show $ map showItemAscii its

-- actions2ascii ac = pp_action ac

-- slrTables2ascii g = ppPTable items2ascii actions2ascii at
--   where g' = expand_cfg g
--         at = e_slr_at g'



-- items2ascii' its = concat $ map showItemAscii its



showSymbLaTeX Dollar   = "dolar"
showSymbLaTeX Root     = "S'"
showSymbLaTeX (T sy)   | head c == '\'' && last c == '\'' = take (l-2) (tail c)
                       | head c == '\"' && last c == '\"' = take (l-2) (tail c)
                       | otherwise = c
  where c = show sy
        l = length c
showSymbLaTeX (NT sy)  | head c == '\'' && last c == '\'' = take (l-2) (tail c)
                       | head c == '\"' && last c == '\"' = take (l-2) (tail c)
                       | otherwise = c
  where c = show sy
        l = length c


showProdLaTeX pr = showSymbLaTeX (lhs_prod pr) ++
                   "\rightarrow " ++ (showLLaTeX (rhs_prod pr))

-- showItemAscii  :: (Show nt , Show t) => Item (Symb t nt) -> String
showItemLaTeX ((h:t),i) = (showSymbLaTeX h) ++ " \rightarrow " ++
       (showLLaTeX $ take (i-1) t) ++ " \textcolor{red}{.} " ++
       (showLLaTeX $ drop (i-1) t)

showLLaTeX []    = ""
showLLaTeX (h:t) = (showSymbLaTeX h) ++ " " ++ (showLLaTeX t)

items2LaTeX its = "$" ++ (concatWithSep (map showItemLaTeX its) " , ") ++ "$"

concatWithSep []     sep = ""
concatWithSep (x:[]) sep = x
concatWithSep (x:xs) sep = x ++ sep ++ concatWithSep xs sep


action2LaTeX (Shift st)    = "\textcolor{green}{Shift} " ++ (items2LaTeX st)
action2LaTeX Accept        = "\textcolor{blue}{Accept}"
action2LaTeX (Reduce prod) = "\textcolor{green}{Reduce} $" ++ (showProdLaTeX prod) ++ "$"
action2LaTeX Error         = "\textcolor{red}{Error}"



-- slrTables2latex g =  ppPTableLaTeX items2LaTeX action2LaTeX at
--   where g' = expand_cfg g
--         at = e_slr_at g'

-- slrTables2latex2file g fn = writeFile fn l
--   where l = slrTables2latex g

