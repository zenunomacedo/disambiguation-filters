-----------------------------------------------------------------------------
-- |
-- Copyright   :  (c) Universidade do Minho, 2004
-- License     :  LGPL
--
-- Maintainer  :  Jo�o Fernandes
-- Stability   :  experimental
-- Portability :  portable
--
-- Incremental generalised Acceptance Function in Haskell
-- with semantic action of ATerm trees and counting its nodes.
--
-----------------------------------------------------------------------------

-- This module uses GHC library module Memo and the function memo that
-- memoises a function call (it uses a hash table to implement the memo
-- table).

{-# LANGUAGE FlexibleContexts #-}

module   Language.HaGLR.G_LR_Inc_Semantics
  ( -- glr_inc_aterm
  -- , glr_inc_xml
  glr_inc_sem
  , glr_inc_sem_res
  -- , xml
  , aterm
  , memo
  ) where

-----------------------------------------------------------------------------

import Data.Char
import Data.List
import Language.HaLex.Dfa
import Language.HaGLR.Semantics
import Language.HaGLR.Stack
import Language.HaGLR.Cfg
import Language.HaGLR.LR_0
import Language.HaGLR.SLR
import Language.HaGLR.G_SLR

import Data.Tree.NTree.TypeDefs

import GHC.Exts
import Control.Monad.Memo
import Debug.Trace

-----------------------------------------------------------------------------

-- glr_inc_aterm :: Cfg Char Char -> [Char] -> [(Bool, NTree String)]
{-glr_inc_aterm
  :: (MonadMemo
        [[Language.HaGLR.LR_0.Item (Symb t nt)]]
        (Action [Language.HaGLR.LR_0.Item (Symb t nt)] [Symb t nt]
         -> [(Bool, NTree String)])
        ((->) [t]),
      Show nt, Show t, Ord nt, Ord t) =>
     Cfg t nt -> [t] -> [(Bool, NTree String)]
glr_inc_aterm g inp = semantic_inc_glraccept aterm g inp


-- | Incremental Generalised LR grammar interpreter that builds an AST
{-semantic_inc_glraccept :: (Eq t,
            Ord t, Ord nt) =>
           (Bool -> ([([Char], [Symb t nt])], Cfg t nt) -> [sem])
           -> Cfg t nt
           -> [t]
           -> [sem]
-}
semantic_inc_glraccept f g inp = glr_inc_sem f sem lookupTT_g lookupAT_g [s] inp
  where eg@(Cfg t nt r p)   = expand_cfg g        -- The expanded grammar
        dfa@(Dfa v q s z d) = ecfg2LR_0Dfa eg     -- The LR_0 DFA
        at                  = e_gslr_at  eg       -- The Action Table
        lookupAT_g          = glookupAT t at      -- The lookup fun. for the AT
        lookupTT_g          = d                   -- The lookup fun. for the TT
        sem                 = ([],g)      -- The information needed to build the XML Tree
-}
-- | Incremental LR Acceptance function
glr_inc_sem_res :: ((Ord (Action a [Symb t2 nt1])), Ord a, Ord t, Ord t2, Ord nt1) =>
        (Bool -> ([(String, [Symb t2 nt1])], Cfg t2 nt1) -> [t1])
     -> ([(String, [Symb t2 nt1])], Cfg t2 nt1)
     -> (a -> Symb t2 nt1 -> a)
     -> (a -> Symb t nt -> [Action a [Symb t2 nt1]])
     -> [a]
     -> [t]
     -> [t1]
glr_inc_sem_res a (sem, g) c d e f = startEvalMemo $ glr_inc_sem a g c d e f sem

glr_inc_sem :: ((Ord (Action a [Symb t2 nt1])), 
                Ord a, 
                Ord t, 
                Ord t2, 
                Ord nt1, 
                MonadMemo ([a], [t], [(String, [Symb t2 nt1])], Action a [Symb t2 nt1]) [t1] m) 
     => (Bool -> ([(String, [Symb t2 nt1])], Cfg t2 nt1) -> [t1])
     -> Cfg t2 nt1
     -> (a -> Symb t2 nt1 -> a)
     -> (a -> Symb t nt -> [Action a [Symb t2 nt1]])
     -> [a]
     -> [t]
     -> [(String, [Symb t2 nt1])]
     -> m [t1]
glr_inc_sem f graph l_tt l_at sk [] semantic = fmap concat $ mapM (for4 memo (glr' f graph l_tt l_at) sk [] semantic) acs
  --[ res
  --                                   | ac <- acs
  --                                   , res <- for3 memo (glr' f semantic l_tt l_at) sk [] ac
  --                                   ]
  where st   = top sk
        acs  = l_at st Dollar

glr_inc_sem f graph l_tt l_at sk inp@(h:t) semantic = fmap concat $ mapM (for4 memo (glr' f graph l_tt l_at) sk inp semantic) acs
--  = [ res
--    | ac <- acs
--    , res <- for3 memo (glr' f semantic l_tt l_at) sk inp ac ]
  where  st = top sk
         acs = l_at st (T h)    -- It returns a list of actions


glr' f g l_tt l_at sk inp semantic (Reduce pr)
  = glr_inc_sem f g l_tt l_at sk'' inp ((prName,pr):semantic)
  where sk'     = popN (sizeProd pr) sk        -- reducing the stack
        st      = top sk'                      -- acessing the state on its top
        st'     = l_tt st (lhs_prod pr)        -- moving to a new state
        sk''    = push st' sk'                 -- pushing the new state
        prName  = prodName pr (prods g)


glr' f g l_tt l_at sk (h:t) semantic (Shift st) = glr_inc_sem f g l_tt l_at sk' t semantic
  where sk'      = push st sk

glr' f g l_tt l_at s inp semantic Accept = return $ f True  (semantic,g)
glr' f g l_tt l_at s inp semantic Error  = return $ f False (semantic,g)

-- aterm True  (semantic, g) = [(True,  toAterm g semantic)]
-- aterm False (semantic, g) = [(False, AAppl "null" [])]
aterm True  (semantic, g) = [(True, toNTreeTest semantic)]
--aterm True  (semantic, g) = [(True, trace (show semantic) $ toNTree g semantic)]
aterm False (semantic, g) = [(False, NTree "invalid parse tree" [])]


