module Filters.Example.DemoLang.Parser where
import Data.List

import Filters.Example.DemoLang.GLR_GoTo
import Filters.Example.DemoLang.GLR_AT
import Filters.Example.DemoLang.Cfg_Grammar
import Language.HaGLR.Cfg 
--import Language.HaGLR.LR_0
import Language.HaGLR.SLR
--import Language.HaGLR.G_LR
import Language.HaGLR.G_SLR
import Language.HaGLR.G_LR_Inc_Semantics
import Language.HaGLR.G_LR_Semantics
--import Language.HaGLR.MemoInc_G_LR
import System.Environment
import Data.Vector (Vector, (!), toList)
import qualified Data.Map as Map

import Data.Tree.NTree.Filter
import Data.Tree.Class


grammar = g

-- compiling: ghc GLRActionsTable.hs -O -main-is GLRActionsTable
main :: IO ()
main = do
  args <- getArgs
  let s = args !! 0
  print $ glr_parser_aterm s
  print $ length $ glr_parser_aterm s

start_state = 0


--prettyPrinting an individual tree. Can be quite hard to read for big trees
--recommended usage: 
-- mapM prettyTree $ disambiguate $ glr_parser_aterm "example input"
prettyTree :: NTree String -> IO ()
prettyTree = putStrLn . formatTree id
lookup_AT :: Map.Map(Symb Char String) Int -> Int -> Symb Char String -> [Action Int [Symb Char String]]
lookup_AT at_col st sy = map fix_reductions $ (at ! st) ! col --  ????
  where col = at_col Map.! sy
        fix_reductions x = case x of 
            Reduce l -> Reduce $ map (\a -> vocabulary_vector ! a) l
            Shift x -> Shift x
            Accept -> Accept
            Error -> Error
glr_parser_aterm inp = glr_sem aterm ([],g) lookup_TT (lookup_AT t_indexes) [start_state] inp

