module Filters.Example where

import Filters.Example.Tiger.Parser as TP
import Filters.Example.DemoLang.Parser as DP
import Filters.Generics
import Filters.Embed
import Filters.Filters

import Data.Tree.NTree.Filter
import Data.Tree.NTree.TypeDefs
import Data.Tree.Class


disambiguation_tiger :: [NTree String] -> [NTree String] 
disambiguation_tiger = (every (gen_filters TP.grammar filters) $$)
 where filters = [
                    LeftAssoc "&" 
                    , ["+"] `Before` ["-"]
                    , ["&"] `After` ["=", ">", "<", ">=", "<="]
                   ]


run_example_tiger :: IO ()
run_example_tiger = do
  s <- readFile "Filters/Example/Tiger/8queen.tiger"
  let work = disambiguation_tiger $ TP.glr_parser_aterm s
  print $ length $ work
  print $ sum $ map cardTree work


disambiguation_demolang :: [NTree String] -> [NTree String] 
disambiguation_demolang = preference "Instr3" "Instr4" . ((every filters) $$ )
 where filters = (follow "Values1" "123456789" 
                 `o` rejectL ["true", "false", "break"] "Id"
                 `o` left_assoc "InfixExp2" 
                 `o` ("InfixExp5" `before` "InfixExp0"))


run_example_demolang :: IO ()
run_example_demolang = do
  s <- readFile "Filters/Example/DemoLang/toy.demolang"
  let work = disambiguation_demolang $ DP.glr_parser_aterm s
  print $ length $ work
  print $ sum $ map cardTree work