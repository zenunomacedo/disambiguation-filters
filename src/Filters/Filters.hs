module Filters.Filters where

import Data.Tree.NTree.Filter
import Data.Tree.NTree.TypeDefs
import Data.Tree.Class
import Filters.Generics
import Language.HaGLR.RegExpAsCfg (implodeSubTree)



left_assoc  p = neg (matches p . head . getChildren . (!!2) . getChildren) `when` matches p
right_assoc p = neg (matches p . head . getChildren . (!!0) . getChildren) `when` matches p

left_assocL p  = neg (matchesL p . head . getChildren . (!!2) . getChildren) `when` matchesL p
right_assocL p = neg (matchesL p . head . getChildren . (!!0) . getChildren) `when` matchesL p

before_l  x y = neg ((matchesL x $$). (concatMap getChildren) . getChildren ) `when` matchesL y
before    x y = neg ((matches  x $$). (concatMap getChildren) . getChildren ) `when` matches  y

reject    w p = neg (matches       (reverse w) . head . getChildren . implodeSubTree) `when` matches p
rejectL  wl p = neg (matchesL (map reverse wl) . head . getChildren . implodeSubTree) `when` matches p

preference bad_token good_token l = preference' l l
 where preference' [] l = []
       preference' (x:xs) l = if any (x `isWorseThan` ) l then preference' xs l else x : preference' xs l
       isWorseThan (NTree x t) (NTree y tt) = if x == y then or (zipWith isWorseThan t tt) else x == bad_token && y == good_token


follow    t r = (firstEndsGood `orElse` secondStartsGood) `when` matches t
 where firstEndsGood    = isOf $ flip notElem r . head . getLastTerm  . (!!0) . getChildren
       secondStartsGood = isOf $ flip notElem r . head . getFirstTerm . (!!1) . getChildren

getLastTerm (NTree [x] []) = [x]
getLastTerm (NTree (x:xs) []) = []
getLastTerm (NTree x t) = if null r then [] else head r
  where tt = map getLastTerm $ reverse t
        r = filter (not . null) tt

getFirstTerm (NTree [x] []) = [x]
getFirstTerm (NTree (x:xs) []) = []
getFirstTerm (NTree x t) = if null r then [] else head r
  where tt = map getFirstTerm $ t
        r = filter (not . null) tt