module Filters.Embed where

import Language.HaGLR.RegExpAsCfg
import Language.HaGLR.Cfg hiding (f, follow)

import Data.Tree.NTree.Filter
import Data.Tree.NTree.TypeDefs
import Data.Tree.Class

import Data.List (isPrefixOf, isSuffixOf)

import Filters.Generics (every)
import Filters.Filters

data Filter t = RightAssoc  t 
              | RightAssocL [t]
              | LeftAssoc   t
              | LeftAssocL  [t]
              | Before [t] [t] 
              | After  [t] [t]
              | NotFollowedBy t t
              | Reject t t
              | Reject_List [t] t
--              | Preference ??

filter2tfilter :: Cfg Char String -> Filter String -> TFilter String
filter2tfilter grammar (LeftAssoc   t)  = seqF [ left_assoc  s | s <- t_prods t grammar]
filter2tfilter grammar (LeftAssocL  t)  = seqF [ left_assocL  s | let s = concatMap (flip t_prods grammar) t]
filter2tfilter grammar (RightAssoc  t) = seqF [ right_assoc s | s <- t_prods t grammar]
filter2tfilter grammar (RightAssocL t) = seqF [ right_assocL s | let s = concatMap (flip t_prods grammar) t]
filter2tfilter grammar (y `After` x) = filter2tfilter grammar (y `Before` x)
filter2tfilter grammar (x `Before` y) = before_l  x' y' 
 where x' = concatMap (flip t_prods grammar) x
       y' = concatMap (flip t_prods grammar) y
filter2tfilter grammar (s `NotFollowedBy` l) = follow s l
filter2tfilter grammar (Reject w t) = reject w t
filter2tfilter grammar (Reject_List wl t) = rejectL wl t

gen_filters :: Cfg Char String -> [Filter String] -> TFilter String
gen_filters grammar = every . seqF . map (filter2tfilter grammar)


t_prods :: String -> Cfg Char String -> [String]
t_prods t g = map fst $ removeWs $ concatMap (\s -> filter (\(a,b) -> s `elem` b) $ prods g) nts  
  where nts                = map (NT . fst) $ find_symbol (map T t)
        find_symbol [s]    = filter (\(a,b) -> null ( tail $ dropWhile (/= s) b)) $ locate_symbol s
        find_symbol (h:xs) = [ xise | 
          xise <- locate_symbol h, 
          xizesse <- find_symbol xs, 
          let resto = tail $ dropWhile (/= h) $ snd xise, 
          not (null resto) && head resto == head (snd xizesse)]

        locate_symbol s    = filter (\(a,b) -> "ntFromT" `isPrefixOf` a && s `elem` b) $ prods g
        removeWs           = filter (not . isSuffixOf "Ws" . fst)
