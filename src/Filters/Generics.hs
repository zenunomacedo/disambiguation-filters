module Filters.Generics where

import Data.Tree.NTree.Filter
import Data.Tree.NTree.TypeDefs
import Data.Tree.Class

--Filter to check if root matches input given
matches :: Eq a => a -> TFilter a
matches c = isOf ((c==) . getNode)

matchesL :: Eq a => [a] -> TFilter a
matchesL l = isOf (flip elem l . getNode)

--Combinator to apply the filter to all nodes, discarding the result if any node fails
every :: TFilter a -> TFilter a
every f = isOf (all (satisfies (every f)) . getChildren) `o` f

--Combinator to apply the filter to all nodes, discarding the result if none of the nodes succeed
some :: TFilter a -> TFilter a
some f = f `orElse` isOf (any (satisfies (some f)) . getChildren)